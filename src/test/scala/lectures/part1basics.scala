package lectures

import org.scalatest.FunSuite
import org.junit.runner.RunWith
import org.scalatestplus.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class part1basics extends FunSuite {

  test("someLibraryMethod is always true") {

    assert(1 == 1)
  }

}
