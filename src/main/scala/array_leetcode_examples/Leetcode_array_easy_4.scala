package array_leetcode_examples

object Leetcode_array_easy_4 {

  def main(args : Array[String]) : Unit={

    /*
    Contains Duplicate:
    Given an array of integers, find if the array contains any duplicates.
    Your function should return true if any value appears at least twice in the array, and it should return false if every element is distinct.
    */

    def containsDuplicate(nums: Array[Int]): Boolean = {

      var my_first_hash_set = collection.immutable.HashSet.empty[Int]

      if (nums.isEmpty) false

      for ( i <- 0 until nums.length)
        {
          if (my_first_hash_set.contains(nums(i))){
            return true
          }
          else{
            if (!my_first_hash_set.contains(nums(i)))
              {
                my_first_hash_set += nums(i)
              }
          }
        }

      print(my_first_hash_set)
      println("containsDuplicate")
      false
    }

    // #################################################################################################################

    val input1 = Array(1,2,3,1)
    val input2 = Array(1,2,3,4)
    val input3 = Array[Int]()
    val result1 = containsDuplicate(input1)
    val result2 = containsDuplicate(input2)
    val result3 = containsDuplicate(input3)
    println(result1)
    println(result2)
    println(result3)

  }
}
