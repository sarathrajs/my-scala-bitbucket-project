package array_leetcode_examples

import scala.collection.mutable._

object Leetcode_array_easy_9 {

  def main(args:Array[String]) : Unit = {

    /*
    Two Sum:
    Given an array of integers nums and an integer target, "return indices" of the two numbers such that they add up to target.
    You may assume that each input would have exactly one solution, and you may not use the same element twice.
    You can return the answer in any order.
    */

    def twoSum(nums: Array[Int], target: Int): Array[Int] = {

      var myMap = Map[Int,Int]()

      for (i <- 0 until nums.length)
        {

          var target_num = target - nums(i)

          if(myMap.contains(target_num))
            {
              return Array(myMap(target_num),i)
            }

          myMap.put(nums(i),i)
        }

      Array(0,0) // You may assume that each input would have exactly one solution
    }

    //####################################################################################################################

    val input1 = Array(2,17,11,27)
    val input2 = 9
    val result = twoSum(input1,input2)
    result.foreach(println)

  }

}
