package array_leetcode_examples

object Leetcode_array_easy_3 {

  def main (args : Array[String]) : Unit = {

    /*
    Rotate Array:
    -------------
    Given an array, rotate the array to the right by k steps, where k is non-negative.
    Follow up:
    Try to come up as many solutions as you can, there are at least 3 different ways to solve this problem.
    Could you do it in-place with O(1) extra space?
    */

    def rotate(nums: Array[Int], k: Int): Array[Int] = {

      reverse(nums,0,nums.length -1 )
      reverse(nums,0,k-1 )
      reverse(nums,k,nums.length -1 )
      println(" Rotate done")
      nums
    }

    def reverse(nums: Array[Int] , s : Int, e : Int) : Array[Int] = {

      var start = s
      var end = e

      while (start < end)
        {
          var temp = nums(start)
          nums(start) = nums(end)
          nums(end) = temp
          start += 1
          end -= 1
        }

      nums

    }

    // #################################################################################################################

    val input = Array(1,2,3,4,5,6,7)
    val input_number = 3
    val result = rotate(input,input_number)

    result.foreach(println)

  }
}
