package array_leetcode_examples

object Leetcode_array_easy_8 {

  def main(args: Array[String]) : Unit = {

    /*
    Move Zeroes:
    Given an array nums, write a function to move all 0's to the end of it while maintaining the relative order of the non-zero elements.

    Example:

    Input: [0,1,0,3,12]
    Output: [1,3,12,0,0]
    Note:

    You must do this in-place without making a copy of the array.
    Minimize the total number of operations.
    */

    def moveZeroes(nums: Array[Int]): Unit = {


      // Base check - 1
      if(nums == null || nums.isEmpty)
        return

      println(" Before Move Zeroes")
      nums.foreach(println)

      var start = 0

      for (i <- 0 until nums.length)
        {
          if(nums(i) != 0)
            {
              var temp = nums(i)
              nums(i) = nums (start)
              nums(start) = temp
              start +=1
            }

        }

      println(" After Move Zeroes")
      nums.foreach(println)
    }

    //####################################################################################################################

    val input = Array(1,1,0,3,12,5,6,7,0,1,2,3,0,5)
    moveZeroes(input)

  }

}
