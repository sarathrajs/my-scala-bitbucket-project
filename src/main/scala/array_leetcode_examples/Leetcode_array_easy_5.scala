package array_leetcode_examples

object Leetcode_array_easy_5 {

  def main (args : Array[String]) : Unit = {

    /*
    Single Number:
    Given a non-empty array of integers nums, every element appears twice except for one. Find that single one.
    Follow up: Could you implement a solution with a linear runtime complexity and without using extra memory?
    */

    def singleNumber(nums: Array[Int]): Int = {

      nums.reduce((a,b) => a ^ b)
    }

    // #################################################################################################################

    val input = Array(2,2,1,1,4,4,5,6,7,6,5)
    val result = singleNumber(input)
    println(result)

  }

}
