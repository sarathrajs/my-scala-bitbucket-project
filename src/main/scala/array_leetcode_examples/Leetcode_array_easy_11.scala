package array_leetcode_examples

object Leetcode_array_easy_11 {

  def main(args:Array[String]) : Unit = {

    /*
    Rotate Image:
    -------------
    Rotate 2D matrix. Rotate in place.
    */

    def rotate(matrix:Array[Array[Int]]) : Unit = {

      println("Before Rotate Matrix : ")
      for (i <- 0 until matrix.length)
        {
          for (j <- 0 until matrix.head.length){
            println(matrix(i)(j))
          }
        }

      var cols = matrix.head.length
      var m = matrix.clone

      (0 until cols).foreach{
        case i => matrix(i) = column(m,i)
      }

      println("After Rotate Matrix")
      for (i <- 0 until matrix.length)
      {
        for (j <- 0 until matrix.head.length){
          println(matrix(i)(j))
        }
      }
    }

    def column(matrix:Array[Array[Int]], num: Int) : Array[Int] = {

      var cols = matrix.head.length

      (cols - 1 to 0 by -1).map{
        case i => matrix(i)(num)
      }.toArray

    }

    //####################################################################################################################

    val input = Array(
      Array(1,2,3),
      Array(4,5,6),
      Array(7,8,9)
    )

    rotate(input)

  }
}
