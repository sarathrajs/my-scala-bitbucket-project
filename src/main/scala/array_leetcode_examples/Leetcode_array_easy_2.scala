package array_leetcode_examples

object Leetcode_array_easy_2 {

  def main(args: Array[String]) : Unit = {

    /* Best Time to Buy and Sell Stock II:

    Say you have an array prices for which the ith element is the price of a given stock on day i.
    Design an algorithm to find the maximum profit. You may complete as many transactions as you like
    (i.e., buy one and sell one share of the stock multiple times).

    Note: You may not engage in multiple transactions at the same time (i.e., you must sell the stock before you buy again).
    */

    def maxProfit(prices: Array[Int]): Int = {

      prices.foreach(println)
      println("Max Profit")

      prices
        .sliding(2,1)
        .map(a => a(1) - a(0))
        .filter(_ > 0)
        .sum

    }

    // #################################################################################################################

    val input = Array (7,1,5,3,6,4)
    val result = maxProfit(input)
    println(result)

  }
}
