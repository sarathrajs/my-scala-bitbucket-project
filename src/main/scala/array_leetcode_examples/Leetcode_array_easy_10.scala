package array_leetcode_examples

import scala.collection.mutable

object Leetcode_array_easy_10 {

  def main (args: Array[String]) : Unit = {

    /*
    Valid Sudoku:
    Determine if a 9 x 9 Sudoku board is valid. Only the filled cells need to be validated according to the following rules:
      Each row must contain the digits 1-9 without repetition.
      Each column must contain the digits 1-9 without repetition.
      Each of the nine 3 x 3 sub-boxes of the grid must contain the digits 1-9 without repetition.
    Note:
      A Sudoku board (partially filled) could be valid but is not necessarily solvable.
      Only the filled cells need to be validated according to the mentioned rules.
    */

    def isValidSudoku(board: Array[Array[Char]]): Boolean = {

      // Always first go for this :
      if (board == null || board.isEmpty || board.head.isEmpty) return false

      var rows = board.length
      var cols = board.head.length

      for(i <- 0 until rows){

        var rowHashSet = mutable.Set[Int]()
        var colHashSet = mutable.Set[Int]()
        var gridHashSet = mutable.Set[Int]()

        for (j <- 0 until cols){

          if(board(i)(j) != '.')
            {
              if (rowHashSet.contains(board(i)(j))){
                println("Loop 1")
                return false
              }
              rowHashSet.add(board(i)(j))
            }

          if(board(j)(i) != '.')
          {
            if (colHashSet.contains(board(j)(i))){
              println("Loop 2")
              return false
            }
            colHashSet.add(board(j)(i))
          }

          if(board(i/3*3 + j/3)(i%3*3 + j%3) != '.')
          {
            if (gridHashSet.contains(board(i/3*3 + j/3)(i%3*3 + j%3))){
              println("Loop 3")
              return false
            }
            gridHashSet.add(board(i/3*3 + j/3)(i%3*3 + j%3))
          }
        }
      }

      println("isValidSudoku with " + rows + " " + cols)
      true
    }

    //####################################################################################################################

    var input = Array(
      Array('5','3','.','.','7','.','.','.','.'),
      Array('6','.','.','1','9','5','.','.','.'),
      Array('.','9','8','.','.','.','.','6','.'),
      Array('8','.','.','.','6','.','.','.','3'),
      Array('4','.','.','8','.','3','.','.','1'),
      Array('7','.','.','.','2','.','.','.','6'),
      Array('.','6','.','.','.','.','2','8','.'),
      Array('.','.','.','4','1','9','.','.','5'),
      Array('.','.','.','.','8','.','.','7','9')
    )

    val result = isValidSudoku(input)
    println(result)

  }

}
