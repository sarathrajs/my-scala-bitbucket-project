package array_leetcode_examples

import scala.util.control.Breaks._
import scala.collection.mutable._

object Leetcode_array_easy_7 {

  def main(args:Array[String]): Unit = {

    /*
    Plus One:
    Given a non-empty array of decimal digits representing a non-negative integer, increment one to the integer.
    The digits are stored such that the most significant digit is at the head of the list,
    and each element in the array contains a single digit.
    You may assume the integer does not contain any leading zero, except the number 0 itself.
    */

    def plusOne(digits: Array[Int]): Array[Int] = {

      var result = digits

      breakable {
        for (i <- digits.length - 1 to 0 by -1) { // by -1 is very important

          if (digits(i) != 9) {
            result(i) += 1
            break
          }
          else
            {
              result(i) = 0
              if (i == 0)
                {
                  result = Array(1) ++ result
                }

            }

        }
      }

      println("Plus one")
      result
    }

    //####################################################################################################################

    val inputs= Array(9,9,9)
    val result = plusOne(inputs)
    result.foreach(println)

  }

}
