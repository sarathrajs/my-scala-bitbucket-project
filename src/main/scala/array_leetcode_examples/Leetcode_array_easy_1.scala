package array_leetcode_examples

object Leetcode_array_easy_1 { // Remove Duplicates from Sorted Array

  def main (args:Array[String]) : Unit = {

    def removeDuplicates(nums: Array[Int]): Int = {

      var start = 0
      var i,j = 0

      for (i <- 1 until nums.length)
        {
          println(s"nums(${i}) = ${nums(i)}")
          if (nums(i) != nums(i-1))
            {
              start += 1
              nums(start) = nums (i)
            }

        }

      start += 1

      var new_array = new Array[Int](start)

      for ( i <- 0 until start) {
        new_array(i) = nums(i)
      }

      new_array.foreach(println)

      println("Value of Start is " + start)
      println("removed duplicates")

      start
    }

    // #################################################################################################################

    val input = Array(0,0,1,1,1,2,2,3,3,4)
    val result = removeDuplicates(input)

    println("Result = " + result)
  }
}
