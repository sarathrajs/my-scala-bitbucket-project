package array_leetcode_examples

import scala.collection.mutable._

object Leetcode_array_easy_6 {

  def main (args:Array[String]) : Unit = {

    /*
    Intersection of Two Arrays II:
    Given two arrays, write a function to compute their intersection.
    Each element in the result should appear as many times as it shows in both arrays.
    The result can be in any order.
    */

    def intersect(nums1: Array[Int], nums2: Array[Int]): Array[Int] = {

      var i,j = 0
      var max_length = Math.max(nums1.length,nums2.length)
      var result = new ArrayBuffer[Int]()

      var hashMap = new HashMap[Int,Int]()
      //var hashMap = collection.mutable.HashMap(4->1)

      println("Before Hashmap: " + hashMap)

      for (i <- 0 until nums1.length)
        {
          if(!hashMap.contains(nums1(i))){
            hashMap ++= HashMap(nums1(i) -> 1)
          }
          else{
            hashMap(nums1(i)) += 1
          }
        }

      println("After Hashmap: " + hashMap)

      println("----------------------- Part 1 Completed -------------------------")

      for (elem <- nums2) {println(elem)}

      for (j <- 0 until nums2.length)
        {
          println ("Value of j is : " + j)
          if(hashMap.contains(nums2(j)) && hashMap(nums2(j)) > 0)
          {
            hashMap(nums2(j)) -= 1
            result += nums2(j)
            result.foreach(println)
          }
        }

      println("----------------------- Part 2 Completed -------------------------")

      result.toArray
    }

    // #################################################################################################################

    val input1 = Array(4,9,5,4,4)
    val input2 = Array(9,4,9,8,4)
    val result = intersect(input1,input2)
    result.foreach(println)

  }
}
