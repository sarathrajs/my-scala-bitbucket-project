package linked_list_leetcode_examples

object Leetcode_linkedlist_easy_2 {

  def main(args: Array[String]): Unit = {

    /*
    Remove Nth Node From End of List:
    Given the head of a linked list, remove the nth node from the end of the list and return its head.
    Follow up: Could you do this in one pass?

    Input: head = [1,2,3,4,5], n = 2
    Output: [1,2,3,5]

    */

    def reverseList(head: ListNode): ListNode = {

      head
    }

    //##################################################################################################################


  }

}
