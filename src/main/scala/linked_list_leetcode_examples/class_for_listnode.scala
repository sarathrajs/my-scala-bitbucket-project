package linked_list_leetcode_examples

class ListNode(var _x: Int = 0) {
  var next: ListNode = null
  var x: Int = _x
}
