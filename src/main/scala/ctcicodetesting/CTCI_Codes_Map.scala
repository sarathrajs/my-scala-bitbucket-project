package ctcicodetesting

object CTCI_Codes_Map {

  /*
  Map is a collection of key-value pairs.
  In other words, it is similar to dictionary.
  Keys are always unique while values need not be unique.
  Key-value pairs can have any data type.
  However, data type once used for any key and value must be consistent throughout.

  Maps are classified into two types:
    ==> mutable.
    ==> immutable.

  By default Scala uses immutable Map.
  In order to use mutable Map, we must import scala.collection.mutable.Map class explicitly.
   */

  //####################################################################################################################

  // How to create Scala Maps:
  // -------------------------

  /* Maps can be created in different ways based upon our requirement and nature of the Map.
     We have different syntax depending upon whether the Map is mutable or immutable.

     Immutable:

        variable = Map(
          key_1 -> value_1,
          key_2 -> value_2,
          key_3 -> value_3, ....)

     Mutable:

        variable = scala.collection.mutable.Map(
          key_1 -> value_1,
          key_2 -> value_2,
          key_3 -> value_3, ....)

  */
  //####################################################################################################################

  // Operations on a Scala Map:
  // -------------------------

  /*
  There are three basic operations we can carry out on a Map:

    keys    : In Scala Map, This method returns an iterable containing each key in the map.
    values  : Value method returns an iterable containing each value in the Scala map.
    isEmpty : This Scala map method returns true if the map is empty otherwise this returns false.

  */

  //####################################################################################################################

  // Accessing Values Using Keys:
  // ----------------------------

  // Values can be accessed using Map variable name and key.

  // Main method
  def main(args:Array[String]) {

    val mapIm = Map(
      "Ajay" -> 30,
      "Bhavesh" -> 20,
      "Charlie" -> 50
    )

    // Accessing score of Ajay
    val ajay = mapIm("Ajay")
    println(ajay)

    // If we try to access value associated with the key “John”, we will get an error because no such key is present in the Map.
    // Therefore, it is recommended to use contains() function while accessing any value using key.
    // This function checks for the key in the Map. If the key is present then it returns true, false otherwise.

    // the key check in the Map
    val bhavesh = if (mapIm.contains("Bhavesh")) mapIm("Bhavesh") else 0

    val john = if (mapIm.contains("John")) mapIm("John") else 0

    println("Bhavesh:" + bhavesh)
    println("John:" + john)


    //####################################################################################################################

    // Updating the values:
    // --------------------

    // If we try to update value of an immutable Map, Scala outputs an error.
    // On the other hand, any changes made in value of any key in case of mutable Maps is accepted.

    // 1. Updating immutable Map:
    // ---------------------------

    // Updating:
    // --------
    // mapIm("Ajay") = 10 // // Error:(101, 5) value update is not a member of scala.collection.immutable.Map[String,Int]
    // println(mapIm)

    // 2. Updating mutable Map:
    // -------------------------
    val mapMut = scala.collection.mutable.Map("Ajay" -> 30,
      "Bhavesh" -> 20,
      "Charlie" -> 50)

    println("Before Updating: " + mapMut)

    // Updating
    mapMut("Ajay") = 10

    println("After Updating: " + mapMut)

    //####################################################################################################################

    // Adding new key-value pair:
    // --------------------------

    // We can insert new key-value pairs in a mutable map using += operator followed by new pairs to be added or updated.

    println("Before Adding: "+mapMut)
    mapMut += ("Ajay" -> 10, "Dinesh" -> 60) // Adding a new key "Dinesh" and updating an existing key "Ajay"
    println("After Adding: "+mapMut)

    //####################################################################################################################

    // Deleting a key-value pair:
    // --------------------------

    // Deleting a key-value pair is similar to adding a new entry.
    // The difference is instead of += we use -= operator followed by keys that are to be deleted.

    println("Before Deleting: "+mapMut)

    // Deleting key-value pairs with
    // keys "Ajay" and "Charlie"
    mapMut -= ("Ajay", "Charlie")

    println("After Deleting: " + mapMut)

    //####################################################################################################################

    // Iteration in a Map:
    // -------------------

    // Key-value pair corresponds to a tuple with two elements.
    // Therefore, while performing iteration loop variable needs to be a pair.

    // (k, v) is a tuple with two elements
    for((key, value) <- mapMut)
    {
      //where k is key and v is value
      print("Key: "+ key +", ")
      println("Value: "+ value)
    }

    //####################################################################################################################

    // Empty Map:
    // ----------

    // In Scala Map, We can also create an empty Map and later add elements to it.

    // Creation of Map having key-value
    // pairs of type (String, Int)
    val mapMutEmpty = scala.collection.mutable.Map[String, Int]()

    println("Empty Map: " + mapMutEmpty)

    // Adding new entry
    mapMutEmpty += ("Charlie" -> 50)

    println("New Entry: " + mapMutEmpty)


  }
}
