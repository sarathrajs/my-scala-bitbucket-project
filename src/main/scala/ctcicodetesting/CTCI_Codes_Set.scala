package ctcicodetesting

// Scala program to illustrate the
// use of immutable set
// For Set-1
import scala.collection.immutable._

object CTCI_Codes_Set {

  def main(args: Array[String])
  {

  /*
  Set in Scala | Set-1 | Immutable Set
  =====================================

  A set is a collection which only contains unique items. The uniqueness of a set are defined by the == method of the type that set holds.
  If you try to add a duplicate item in the set, then set quietly discard your request.

  Syntax:

        // Immutable set:
        -----------------
        val variable_name: Set[type] = Set(item1, item2, item3)
        or
        val variable_name = Set(item1, item2, item3)

        // Mutable Set:
        //-------------
        var variable_name: Set[type] = Set(item1, item2, item3)
        or
        var variable_name = Set(item1, item2, item3)

  Some Important Points about Set in Scala:
  -----------------------------------------
        * In Scala, both mutable and immutable sets are available.
            Mutable set is those set in which the value of the object is change but,
            in the immutable set, the value of the object is not changed itself.

        * By default set in Scala are immutable.

        * In Scala, the immutable set is defined under
            Scala.collection.immutable._ package and mutable set are defined under
            Scala.collection.mutable._   package.

        * We can also define a mutable set under Scala.collection.immutable._ package as shown in the below example.
        * A Set has various methods to add, remove clear, size, etc. to enhance the usage of the set.
        * In Scala, We are allowed to create empty set.

        Syntax:
        // Immutable empty set
        val variable_name = Set()

        // Mutable empty set
        var variable_name = Set()
  */

    // Creating and initializing immutable sets:
    // -----------------------------------------

    val myset1: Set[String] = Set("Geeks", "GFG", "GeeksforGeeks", "Geek123")

    val myset2 = Set("C", "C#", "Java", "Scala", "PHP", "Ruby")

    val myemptyset = Set()

    // Display the value of myset1:
    // ----------------------------
    println("Set 1:")
    println(myset1)

    // Display the value of myset2 using for loop:
    // -------------------------------------------
    println("\nSet 2:")
    for(myset <- myset2)
    {
      println(myset)
    }

    // Display the value of myset1 using a foreach loop:
    // -------------------------------------------------
    myset1.foreach(
      item => println(item)
    )

    // Display the value of myemptyset:
    // --------------------------------
    println("The empty set is:")
    println(myemptyset)

    //####################################################################################################################

    // Sorted Set:
    // -----------

    // In Set, SortedSet is used to get values from the set in sorted order. SortedSet is only work for immutable set.

    val myset: SortedSet[Int] = SortedSet(87, 0, 3, 45, 7, 56, 8,6)

    myset.foreach(
      items => println(items)
    )

    //####################################################################################################################

    // Adding items in Mutable Set in Mutable collection:
    // --------------------------------------------------

    // Scala program to illustrate how to
    // add items using +=, ++== and add()
    // method in mutable set with mutable
    // collection
    import scala.collection.mutable._

    // In Set, We can only add new elements in mutable set. +=, ++== and add() method is used to add new elements when
    // we are working with mutable set in mutable collection and += is used to add new elements when we are working with
    // mutable set in immutable collection.

    // Creating and initilazing set
    println("==============================")

    var myset_new = Set("G", "Geek", "for")

    println("Set before addition " +
      "of new elements:")

    println(myset_new)

    // Adding new element in set
    // using += and ++==
    myset_new += "Geeks"

    // Here, "G" is already present in the
    // Set so, "G" is not added in set
    myset_new ++== List("Geeks12", "geek23", "G")

    // Adding elements using add() method
    myset_new.add("GeeksforGeeks")
    myset_new.add("geeksForgeeks100")
    println("\nSet after addition of new elements:")
    println(myset_new)

    //-------------------------------------------------------------------------------------------------

    // Adding items in Mutable Set in immutable collection:
    // ----------------------------------------------------

    // Scala program to illustrate how
    // to add items using += operator in
    // mutable set with immutable collection
    import scala.collection.immutable._

    var myset_new_2 = scala.collection.immutable.Set("G", "Geek", "for")

    println("Set before addition" +
      " of new elements:")
    println(myset_new_2)

    // Adding new element in set
    // using += operator
    myset_new_2 += "GeeksforGeeks"
    myset_new_2 += "geeks1000"

    println("\nSet after addition " +
      "of new elements:")
    println(myset_new_2)

    //####################################################################################################################

    // Adding items in immutable Set:
    // ------------------------------

    /*
    In immutable set, We cannot add elements, but we can use + and ++ operators to add element from the immutable set
    and store the result into a new variable.
    Here, + is used to add single or multiple elements and ++ is used to add multiple elements defined in another
    sequence and in concatenation of immutable set.
    */

    // Scala program to illustrate how
    // to add elements in immutable set
    import scala.collection.immutable._

    // Creating and initilazing immutable set:
    // ---------------------------------------

    val myset11 = scala.collection.immutable.Set(100, 400, 500, 600,300, 800)

    val myset22 = scala.collection.immutable.Set(11, 44, 55, 66, 77)

    println("=======================================================")
    println("Set before addition:")
    println(myset11)
    println(myset22)
    println("\nSet after addition:")

    // Add single element in myset1
    // and create new Set
    val S1 = myset11 + 900
    println(S1)

    // Add multiple elements in myset1
    // and create new Set
    val S2 = myset11 + (200, 300)
    println(S2)

    // Add another list into myset1
    // and create new Set
    val S3 = myset11 ++ List(700, 1000)
    println(S3)

    // Add another set myset2 into
    // myset1 and create new Set
    val S4 = myset11 ++ myset22
    println(S4)

    //####################################################################################################################

    // Removing elements from the immutable set:
    // -----------------------------------------

    // In immutable set, We cannot remove elements, but we can use – and — operators to remove elements from the
    // immutable set and store the result into a new variable.
    // Here, – operator is used to remove one or more elements and — operator is used to remove
    // multiple elements defined in another sequence.

    // Scala program to illustrate how
    // to remove elements in immutable set
    import scala.collection.immutable._

    // Creating and initilazing
    // immutable set

    val mysetz = scala.collection.immutable.Set(100, 400, 500, 600, 300, 800, 900, 700)
    println("Set before deletion:")
    println(mysetz)

    println("\nSet after deletion:")

    // Remove single element in myset and
    // Result store into new variable
    val S1z = mysetz - 100
    println(S1z)

    // Remove multiple elements from myset
    // Result store into new variable
    val S2z = mysetz - (400, 300)
    println(S2z)

    // Remove another list from myset
    // Result store into new variable
    val S3z = mysetz -- List(700, 500)
    println(S3z)

    //####################################################################################################################

    // Set Operations:
    // ---------------

    // Now we will see some of the basic mathematical operations on the Set like Union, Intersection, and Difference.

    //    1. Union: In this, we could simply add one Set with other.
    //              Since the Set will itself not allow any duplicate entries, we need not take care of the common values.
    //              To perform union, we use union() method.

    //    2. Intersection: To get the common values from both Sets we use intersect() method.
    //                     It returns a new set which contains all the common values present in both sets.

    //    3. Difference: To get the difference of two Sets we use diff() method.
    //                   It returns the set which contains all the that are not present in myset2.

    // Scala program to illustrate union,
    // intersection, and difference on Set
    import scala.collection.immutable._

    // Creating and initializing set
    val myset1_math = scala.collection.immutable.Set(11, 22, 33, 44,55, 66, 77, 111)
    val myset2_math = scala.collection.immutable.Set(88, 22, 99, 44, 55, 66, 77)

    // To find intersection
    val S1_math = myset1_math.intersect(myset2_math)
    println("Intersection:")
    println(S1_math)

    // To find the symmetric difference
    val S2_math = myset1_math.diff(myset2_math)
    println("\nDifference:")
    println(S2_math)

    // To find union
    val S3_math = myset1_math.union(myset2_math)
    println("\nUnion:")
    println(S3_math)

  }
}
