package ctcicodetesting

import scala.collection.mutable.HashMap // Scala program to create or print HashMap

object CTCI_Codes_HashMap {

  def main(args: Array[String]): Unit = {

    println("CTCI .. Lets get started!!")

    // Creating empty HashMap
    var hashMap = new HashMap()

    // Creating HashMap with values
    var hashMap2 = HashMap("C"->"Csharp", "S"->"Scala", "J"->"Java")

    // Printing HashMap
    println(hashMap)
    println(hashMap2)
    println(hashMap2.keys)
    println(hashMap2.values)

    println("------------------------------------------")

    // Iterating elements:
    hashMap2.foreach
    {
      case (key, value) => println (key + " -> " + value)
    }

    println("------------------------------------------")

    // Accessing value by using key
    println(hashMap2("S"))

    println("------------------------------------------")

    // Adding element
    var HashMap3 = hashMap2 + ("P"->"Perl")

    HashMap3.foreach
    {
      case (key, value) => println (key + " -> " + value)
    }

    println("------------------------------------------")

    // Removing an element
    HashMap3 -= "C"

    println("After Removing")
    HashMap3.foreach
    {
      case (key, value) => println (key + " -> " + value)
    }

    println("------------------------------------------")

    // How to add elements from a HashMap to an existing HashMap using ++=
    // The code below shows how to add elements from a HashMap to an existing HashMap using ++=.

    println("How to add elements from a HashMap to an existing HashMap using ++=")
    HashMap3 ++= hashMap2
    println(s"Elements in hashMap1 = $HashMap3")

    println("------------------------------------------")

  }

}
