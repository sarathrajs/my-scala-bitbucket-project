package ctcicodetesting

object CTCI_Codes_StringBuilder {

  def main (args: Array[String]) : Unit = {

    /*
    StringBuilder in Scala:
    -----------------------

    A String object is immutable, i.e. a String cannot be changed once created.
    In situations where you need to perform repeated modifications to a string, we need StringBuilder class.
    StringBuilder is utilized to append input data to the internal buffer.
    We can perform numerous operations with the support of methods on the StringBuilder.
    This operation comprises appending data, inserting data, and removing data.

    Important points:

        The StringBuilder class is beneficent for mutable strings to extend effectively.
        The instance of StringBuilder is utilized like a String.
        Strings of Scala are immutable so, when you require a mutable String then you can use StringBuilder.
    */

    //####################################################################################################################

    // Operations performed by the StringBuilder class:
    // ================================================

    // 1. Appending character:
    // This operation is helpful in appending character.
    // Here, (x += ‘ ‘) is utilized to append a character.

    // Creating StringBuilder
    val x = new StringBuilder("Author")

    // Appending character
    val y = (x += 's')

    // Displays the string after
    // appending the character
    println(y)


    // 2. Appending String:
    // --------------------
    // This operation is helpful in appending string.
    // Here, (x ++= ‘ ‘) is utilized to append String.

    // Creating StringBuilder
    val a = new StringBuilder("Authors")

    // Appending String
    val b = (a ++= " of GeeksforGeeks")

    // Displays the string after
    // appending the string
    println(b)

    // 3. Appending String representation of number:
    // ----------------------------------------------
    // Here, the number can be of any type like Integer, Double, Long, Float, etc.
    // Here, x.append(n) is utilized to append the String representation of the number, where ‘n’ is the number of any type.

    // Creating StringBuilder
    val c = new StringBuilder("Number of Contributors : ")

    // Appending String
    // representation of number
    val d = c.append(800)

    // Displays the string after
    // appending the number
    println(d)

    // 4. Resetting the content of the StringBuilder:
    // ----------------------------------------------
    // It is helpful in resetting the content by making it empty.
    // Here, x.clear() is utilized to clear the content of the StringBuilder.

    // Creating StringBuilder
    val e = new StringBuilder("Hello")

    // Resetting the content
    val f = e.clear()

    // Displays empty content
    println(f)

    // 5. Delete operation:
    // ---------------------
    // This operation is helpful in deleting characters from the content of the StringBuilder.
    // Here, q.delete(i, j) is utilized to delete the character indexed from i to (j – 1).

    // Creating StringBuilder
    val q = new StringBuilder("Computer Science")

    // Deleting characters
    val r = q.delete(1, 3)

    // Displaying string after
    // deleting some characters
    println(r)

    // 6. Insertion operation:
    // -----------------------
    // This operation is helpful in inserting Strings
    // Here, q.insert(i, “s”) is utilized to insert the String (s) at index i.

    // Creating StringBuilder
    val s = new StringBuilder("GfG CS portal")

    // inserting strings
    val t = s.insert(4, "is a " )

    // Displays string after
    // insertion of required
    // string
    println(t)

    // 7. Converting StringBuilder to a String:
    // ----------------------------------------
    // StringBuilder can be converted to a String using this operation.
    // Here, q.toString is utilized to convert StringBuilder to a string.

    // Creating StringBuilder
    val u = new StringBuilder("GeeksforGeeks")

    // Applying conversion
    // operation
    val v = u.toString

    // Displays String
    println(v)

  }

}
