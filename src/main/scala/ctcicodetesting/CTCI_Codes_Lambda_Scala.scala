package ctcicodetesting

object CTCI_Codes_Lambda_Scala {

  def main (args : Array[String]) : Unit = {

    /*
    Lambda Expression in Scala:
    ---------------------------

    Lambda Expression refers to an expression that uses an anonymous function instead of variable or value.
    Lambda expressions are more convenient when we have a simple function to be used in one place.
    These expressions are faster and more expressive than defining a whole function.
    We can make our lambda expressions reusable for any kind of transformations.
    It can iterate over a collection of objects and perform some kind of transformation to them.

    Example:

        // lambda expression to find double of x
        val ex = (x:Int) => x + x
    */

    // lambda expression 1:
    // --------------------
    val ex1 = (x:Int) => x + 2

    // with multiple parameters
    val ex2 = (x:Int, y:Int) => x * y

    println(ex1(7))
    println(ex2(2, 3))

    // lambda expression 2:
    // --------------------
    // list of numbers
    val l = List(1, 1, 2, 3, 5, 8)

    // squaring each element of the list
    val res = l.map( (x:Int) => x * x )

    /* OR
    val res = l.map( x=> x * x )
    */
    println(res)

    // lambda expression 3:
    // --------------------
    // list of numbers
    val l1 = List(1, 1, 2, 3, 5, 8)
    val l2 = List(13, 21, 34)

    // reusable lambda
    val func = (x:Int) => x * x

    // squaring each element of the lists
    val res1 = l1.map( func )
    val res2 = l2.map( func )

    println(res1)
    println(res2)

  }
}
