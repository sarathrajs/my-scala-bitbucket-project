package ctcicodetesting

// Scala program of Initializing TreeSet
import scala.collection.immutable.TreeSet

object CTCI_Codes_TreeSet {

  def main (args : Array[String]) : Unit = {

    /*
    TreeSet in Scala:
    -----------------

    Set is a data structure which allows us to store elements which are unique.
    The ordering of elements does not guarantee by the Set, than a TreeSet will make elements in a given order.
    In Scala, TreeSet have two versions:
              scala.collection.immutable.TreeSet
              scala.collection.mutable.TreeSet
    */

    //####################################################################################################################

    // Operations perform with TreeSet:
    // ================================

    // 1 .Initialize a TreeSet :
    // --------------------------
    // Below is the example to create or initialize TreeSet.

    println("Initialize a TreeSet")

    // Creating TreeSet
    val treeSet: TreeSet[String] = TreeSet("Geeks", "GeeksForGeeks", "Author")
    println(s"Elements are = $treeSet")

    // 2. Check specific elements in TreeSet :
    // ---------------------------------------

    // Checking
    println(s"Element Geeks = ${treeSet("Geeks")}")
    println(s"Element Student = ${treeSet("Student")}")

    // 3. Adding an element in TreeSet :
    // ---------------------------------

    // Adding an element in HashSet
    val ts1: TreeSet[String] = treeSet + "GeeksClasses"
    println(s"Adding elements to TreeSet = $ts1")

    // 4. Adding two TreeSets in TreeSets :
    // ------------------------------------
    //We can add two TreeSets by using ++ sign. below is the example of adding two TreeSets.

    // Adding elements in HashSet
    val ts2: TreeSet[String] = ts1 ++ TreeSet[String]("Java", "Scala")
    println(s"Add more than one TreeSets = $ts2")

    // 5. Remove element in TreeSet:
    // -----------------------------
    // We can remove an element in TreeSet by using – sign. below is the example of removing an element in TreeSet.

    // removing elements in HashSet
    val ts3: TreeSet[String] = ts2 - "Geeks"
    println(s"remove element from treeset = $ts3")

    // 6. Find the intersection between two TreeSets:
    // ----------------------------------------------
    // We can find intersection between two TreeSets by using & sign. below is the example of finding intersection between two TreeSets.

    println("Initialize two TreeSets")

    // Creating two TreeSet
    val ts4: TreeSet[String] = TreeSet("Geeks", "GeeksForGeeks", "Author")
    println(s"Elements of treeset1 are = $ts4")

    val ts5: TreeSet[String] = TreeSet("Java", "Geeks", "Scala")
    println(s"Elements of treeset2 are = $ts5")

    // finding the intersection between two TreeSets
    println(s"Intersection of treeSet1 and treeSet2 = ${ts4 & ts5}")

    // 7. Initializing an empty TreeSet :
    // ----------------------------------
    // Below is the example to show empty TreeSet.

    // Initializing an empty TreeSet
    val emptyTreeSet: TreeSet[String] = TreeSet.empty[String]
    println(s"Empty TreeSet = $emptyTreeSet")


  }

}
