package ctcicodetesting

// Scala program to print immutable lists
import scala.collection.immutable._

object CTCI_Codes_Lists extends App{

  /*

  A list is a collection which contains immutable data.
  List represents linked list in Scala. The Scala List class holds a sequenced, linear list of items.
  Following are the point of difference between lists and array in Scala:

    Lists are immutable whereas arrays are mutable in Scala.
    Lists represents a linked list whereas arrays are flat.

  */

  val mylist1: List[String] = List("Geeks", "GFG", "GeeksforGeeks", "Geek123")

  val mylist2 = List("C", "C#", "Java", "Scala", "PHP", "Ruby")

  // Display the value of mylist1
  println("List 1:")
  println(mylist1)

  // Display the value of mylist2 using for loop
  println("\nList 2:")

  for(value <- mylist2)
  {
    println(value)
  }

  // Creating an Empty List:
  //------------------------
  val emptylist: List[Nothing] = List()
  println("The empty list is:")
  println(emptylist)

  // Scala program to illustrate the use of two dimensional list:
  //-------------------------------------------------------------

  // Creating a two-dimensional List.
  val twodlist: List[List[Int]] =
    List(
      List(1, 0, 0),
      List(0, 1, 0),
      List(0, 0, 1)
    )
  println("The two dimensional list is:")
  println(twodlist)

  //####################################################################################################################

  // Basic Operations on Lists:
  //---------------------------

  // The following are the three basic operations which can be performed on list in scala:

  // 1. head: The first element of a list returned by head method.

  println("--- Printing Heads ---")
  println(mylist1.head)
  println(mylist2.head)
  //println(emptylist.head) // Exception in thread "main" java.util.NoSuchElementException: head of empty list
  println(twodlist.head)

  // 2. tail: This method returns a list consisting of all elements except the first / head.

  println("--- Printing Tails ---")
  println(mylist1.tail)
  println(mylist2.tail)
  //println(emptylist.tail) // Exception in thread "main" java.util.NoSuchElementException: head of empty list
  println(twodlist.tail)

  // 3. isEmpty: This method returns true if the list is empty otherwise false

  println(emptylist.isEmpty)

  //####################################################################################################################

  // How to create a uniform list in Scala:
  // ---------------------------------------
  //Uniform List can be created in Scala using List.fill() method.
  // List.fill() method creates a list and fills it with zero or more copies of an element.

  // Repeats Scala three times.
  val programminglanguage = List.fill(3)("Scala")
  println( "Programming Language : " + programminglanguage )

  // Repeats 4, 8 times.
  val number= List.fill(8)(4)
  println("number : " + number)

  //####################################################################################################################

  // Reversing List Order in Scala:
  // ------------------------------
  //The list order can be reversed in Scala using List.reverse method.
  // List.reverse method can be used to reverse all elements of the list.

  println("Reversing List")
  println(mylist2)
  println(mylist2.reverse)

}
