package ctcicodetesting

object CTCI_Codes_ListMap {

  def main (args: Array[String]): Unit = {

    /*
    ListMap in Scala:
    -----------------

    Immutable maps Implemented by using a list-based data structure.
    The Scala List class holds a sequenced, linear list of items.
    We must import scala.collection.mutable.ListMap for ListMap.
    ListMap collection used only for a small number of elements.

        Syntax:

        var listMapName = ListMap("k1"->"v1", "k2"->"v2", "k3"->"v3", ...)
        Here, k is key and v is value.
    */

    //####################################################################################################################

    // Basic Operations on ListMap:
    // ============================

    // 1. Creating ListMap with values:
    // --------------------------------

    var listMap = scala.collection.immutable.ListMap("C"->"Csharp", "S"->"Scala", "J"->"Java")

    // Printing ListMap
    println(listMap)

    //####################################################################################################################

    // 2. Adding and accessing elements:
    // ---------------------------------
    // A ListMap is created, add elements and access elements also performed.

    // Creating ListMap:
    var listMap2 = scala.collection.mutable.ListMap("C"->"Csharp", "S"->"Scala", "J"->"Java")

    // Iterating elements:
    listMap2.foreach
    {
      case (key, value) => println (key + " -> " + value)
    }

    // Accessing value by using key:
    println(listMap2("S"))

    // Adding element:
    var ListMap3 = listMap2 + ("P"->"Perl")

    ListMap3.foreach
    {
      case (key, value) => println (key + " -> " + value)
    }

    //####################################################################################################################

    // 3. Removing an element from ListMap:
    // ------------------------------------
    // A ListMap is created than removing an element is performed using – sign.
    // Below is the example to removing an element from ListMap.

    // Creating ListMap
    var listMap_remove = scala.collection.mutable.ListMap("C"->"Csharp", "S"->"Scala", "J"->"Java")

    // Iterating elements
    listMap_remove.foreach
    {
      case (key, value) => println (key + " -> " + value)
    }

    // Removing an element
    listMap_remove -= "C"

    println("After Removing")
    listMap_remove.foreach
    {
      case (key, value) => println (key + " -> " + value)
    }

    //####################################################################################################################

    // 4. Creating an empty ListMap:
    // -----------------------------
    // An empty ListMap is created either by calling its constructor or using ListMap.empty method

    // Creating an empty list map by calling constructor
    var emptyListMap1 = new scala.collection.mutable.ListMap()

    // Creating an empty list map by using .empty method
    var emptyListMap2 = scala.collection.mutable.ListMap.empty

    // Printing empty ListMap
    println(emptyListMap1)
    println(emptyListMap2)





  }

}
