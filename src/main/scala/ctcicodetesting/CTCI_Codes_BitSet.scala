package ctcicodetesting

import scala.collection.immutable.BitSet

object CTCI_Codes_BitSet {

  def main(args: Array[String]) : Unit = {

    /*
    BitSet in Scala:
    ----------------
    A set is a collection which only contains unique items which are not repeatable.
    A BitSet is a collection of small integers as the bits of a larger integer.
    Non negative integers sets which represented as array of variable-size of bits packed into 64-bit words is called BitSets.
    The largest number stored in bitset is the memory of a bitset. It extends Set trait.

        Syntax:
        var BS : BitSet = BitSet(element1, element2, element3, ....)
        Where BS is the name of created BitSet

    In Scala, BitSet have two versions:
            scala.collection.immutable.BitSet
            scala.collection.mutable.BitSet

    They are almost identical but the mutable version changes the bits in place so immutable data structures are much better for concurrency.
   */

    //####################################################################################################################

    // Operations perform with BitSet:
    // ===============================

    // 1. Initialize a BitSet :
    // ------------------------
    // Below is the example to create or initialize BitSet.

    println("Initialize a BitSet")

    // Creating HashSet
    val bitSet: BitSet = BitSet(0, 1, 2, 3)
    println(s"Elements are = $bitSet")

    //####################################################################################################################

    // 2. Check specific elements in BitSet :
    // --------------------------------------

    // Checking
    println(s"Element 2 = ${bitSet(2)}") // Element 2 = true
    println(s"Element 4 = ${bitSet(4)}") // Element 4 = false

    //####################################################################################################################

    // 3. Adding an elements in BitSet :
    // ---------------------------------
    // We can add an element in BitSet by using + sign. below is the example of adding an element in BitSet.

    // Adding an element in BitSet
    val bs1: BitSet = bitSet + 10 + 11
    println(s"Adding elements to BitSet = $bs1")

    //####################################################################################################################

    // 4. Adding more than one element in BitSet :
    // --------------------------------------------
    // We can add more than one element in BitSet by using ++ sign. below is the example of adding more than one elements in BitSet.

    // Adding elements in BitSet
    val bs2: BitSet = bs1 ++ BitSet(4, 5, 6)
    println(s"Add more than one elements to BitSet = $bs2")

    //####################################################################################################################

    // 5. Remove element in BitSet :
    // -----------------------------
    // We can remove an element in BitSet by using – sign. below is the example of removing an element in BitSet.

    // removing elements in BitSet
    val bs3: BitSet = bs2 - 2
    println(s"remove element from bitset = $bs3")

    //####################################################################################################################

    // 6. Find the intersection between two BitSets :
    // ----------------------------------------------
    // We can find intersection between two BitSets by using & sign. below is the example of finding intersection between two BitSets.

    // Creating two BitSet
    val bs4: BitSet = BitSet(0, 1, 2, 3)
    println(s"Elements of bitset1 are = $bs4")

    val bs5: BitSet = BitSet(4, 5, 3, 6)
    println(s"Elements of bitset2 are = $bs5")

    // finding the intersection between two BitSets
    println(s"Intersection of bitSet1 and bitSet2 = ${bs4 & bs5}")

    //####################################################################################################################

    // 7. Initializing an empty BitSet :
    // ---------------------------------

    // Initializing an empty BitSet
    val emptyBitSet: BitSet = BitSet.empty
    println(s"Empty BitSet = $emptyBitSet")

  }
}
