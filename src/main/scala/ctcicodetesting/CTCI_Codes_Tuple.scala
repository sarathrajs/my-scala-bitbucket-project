package ctcicodetesting

object CTCI_Codes_Tuple extends App{

  /* TUPLE:
  ---------
  Tuple is a collection of elements.
  Tuples are heterogeneous data structures, i.e., is they can store elements of different data types.
  A tuple is immutable, unlike an array in scala which is mutable.
  An example of a tuple storing an integer, a string, and boolean value.

    val name = (15, "Chandan", true)

  Type of tuple is defined by, the number of the element it contains and datatype of those elements.

  For Example:

    // this is tuple of type Tuple3[ Int, String, Boolean ]
    val name = (15, "Chandan", true)

  Let N be the number of elements in a tuple.
  Scala currently is limited to 22 elements in a tuple, that is N should be, 1<=N<=22 , the tuple can have at most 22 elements,
  if the number of elements exceeds 22 then this will generate an error.
  However we can use nested tuples to overcome this limit (Note that a tuple can contain other tuples)

  //####################################################################################################################

  // Operations on tuple:
  // --------------------

  1. Access element from tuple:
  -----------------------------
  Tuple elements can be accessed using an underscore syntax, method tup._i is used to access the ith element of the tuple.

  */

  var name = (15, "chandan", true)

  println(name._1) // print 1st element
  println(name._2) // print 2st element
  println(name._3) // print 3st element

  // 2. Pattern matching on tuples:
  // ------------------------------
  // Pattern matching is a mechanism for checking a value against a pattern.
  // A successful match can also deconstruct a value into its constituent parts.:

  var (roll_no, name_of_the_student, pass) = (15, "chandan", true)
  println(roll_no)
  println(name_of_the_student)
  println(pass)

  // 3. Iterating over a tuple:
  // --------------------------
  // To iterate over tuple, tuple.productIterator() method is used.

  println("===================")
  name.productIterator.foreach{
    i=>println(i)
  }

  // 4. Converting tuple to string:
  // ------------------------------
  // Converting a tuple to a string concatenates all of its elements into a string.
  // We use the tuple.toString() method for this.

  println("===================")
  println(name) // print tuple
  println(name.toString()) // print converted string

  // 5. Swap the elements of tuple:
  // ------------------------------
  // Swapping the element of a tuple we can use tuple.swap Method.

  val name_swap = ("geeksforgeeks","gfg")

  // print swapped element
  println(name_swap.swap)
}
