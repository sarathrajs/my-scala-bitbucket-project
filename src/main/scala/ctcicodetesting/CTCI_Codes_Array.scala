package ctcicodetesting

// Scala program to concatenate two array
// by using concat() method
import Array._

object CTCI_Codes_Array {

  def main(args: Array[String]) {

    /*

    Array is a special kind of collection in scala.
    It is a fixed size data structure that stores elements of the same data type.
    The index of the first element of an array is zero and the last element is the total number of elements minus one.
    It is a collection of mutable values.
    It corresponds to arrays(in terms of syntax) in java but at the same time it’s different(in terms of functionalities) from java.

    Some Important Points:
    ----------------------
      * Scala arrays can be generic. which mean we can have an Array[T], where T is a type parameter or abstract type.
      * Scala arrays are compatible with Scala sequences – we can pass an Array[T] where a Seq[T] is required.
      * Scala arrays also support all sequence operations.

    The following figure shows how values can be stored in array sequentially :
    ----------------------------------------------------------------------------
      40 , 55 , 45, 43, 88, 92
      0    1    2   3   4   5  <- Array Indices

      Array Length = 6
      First Index = 0
      Last Index = 5
    */

    //####################################################################################################################

    // One Dimensional Array:
    // ----------------------

    // In this array contains only one row for storing the values.
    // All values of this array are stored contiguously starting from 0 to the array size.

    // var arrayname = new Array[datatype](size) ==> Here, datatype specifies the type of data being allocated, size specifies the
    // number of elements in the array, and var is the name of array variable that is linked to the array.

    // allocating memory of 1D Array of string.
    var days = Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" )

    println("Array elements are : ")

    for ( m1 <-days )
    {
      println(m1 )
    }

    //####################################################################################################################

    // Basic Operation On An Array:
    // =============================

    // 1. Accessing array elements:
    // -----------------------------
    println(days(1))

    // 2. Updating an element in array:
    // --------------------------------
    days(1) = "Mondayyyyyy"
    println(days(1))

    // 3. Adding elements in an array:
    // --------------------------------
    //days(7) = "Eight random day" // Exception in thread "main" java.lang.ArrayIndexOutOfBoundsException: 7
    // since we are accessing an element not within this array it throws this error. lets create a new one
    var name = new Array[String](4)
    name(0)="gfg"
    name(1)="geeks"
    name(2)="GeeksQuize"
    name(3)="geeksforgeeks"
    println("After adding array elements : ")

    for ( m1 <-name )
    {
      println(m1 )
    }

    // 4. Concatenate Arrays:
    // ----------------------

    // We can concatenate two arrays by using concat() method. In concat() method we can pass more than one array as arguments.

    var arr1 = Array(1, 2, 3, 4)
    var arr2 = Array(5, 6, 7, 8)

    var arr3 = concat( arr1, arr2)

    // Print all the array elements
    for ( x <- arr3 )
    {
      println( x )
    }

    //####################################################################################################################

    // Multidimensional Arrays:
    // ------------------------

    /*
    The Multidimensional arrays contains more than one row to store the values.
    Scala has a method Array.ofDim to create Multidimensional arrays in Scala .
    In structures like matrices and tables : multi-dimensional arrays can be used.

      var array_name = Array.ofDim[ArrayType](N, M)
      or
      var array_name = Array(Array(elements), Array(elements)

    This is a Two-Dimension array. Here N is no. of rows and M is no. of Columns.
    */

    val rows = 2
    val cols = 3

    // Declaring Multidimensional array
    val names = Array.ofDim[String](rows, cols)

    // Allocating values
    names(0)(0) = "gfg"
    names(0)(1) = "Geeks"
    names(0)(2) = "GeeksQuize"
    names(1)(0) = "GeeksForGeeks"
    names(1)(1) = "Employee"
    names(1)(2) = "Author"

    for {
      i <- 0 until rows
      j <- 0 until cols
    } println(s"($i)($j) = ${names(i)(j)}") // Printing values

    //####################################################################################################################

    // Append and Prepend elements to an Array in Scala:
    // -------------------------------------------------

    /*
    Use these operators (methods) to append and prepend elements to an array while assigning the result to a new variable:

    Method	        Function	                    Example
    --------------------------------------------------------------------
    :+	            append 1 item	                old_array :+ e
    ++	            append N item	                old_array ++ new_array
    +:	            prepend 1 item	              e +: old_array
    ++:	            prepend N items	              new_array ++: old_array

    Examples to show how to use the above methods to append and prepend elements to an Array:
    */

    // Declaring an array
    val a = Array(45, 52, 61)
    println("Array a ")
    for ( x <- a )
    {
      println( x )
    }

    // Appending 1 item
    val b = a :+ 27
    println("Array b ")
    for ( x <- b )
    {
      println( x )
    }

    // Appending 2 item
    val c = b ++ Array(1, 2)
    println("Array c ")
    for ( x <- c )
    {
      println( x )
    }

    // Prepending 1 item
    val d = 3 +: c
    println("Array d ")
    for ( x <- d )
    {
      println( x )
    }

    // Prepending 2 item
    println("Array e ")
    val e = Array(10, 25) ++: d
    for ( x <- e )
    {
      println( x )
    }

  }
}
