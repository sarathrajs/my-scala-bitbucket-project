package ctcicodetesting

import java.util.ArrayList

object CTCI_Codes_ArrayList {

  def main(args: Array[String]): Unit = {

    // Example 1:
    // -----------

    // creating array list object
    var list = new ArrayList[String]();

    // adding elements
    for (a <- 65 to 90) {
      list.add("" + (a.asInstanceOf[Char]) + ""); // type casting int to char
    }

    // printing the list
    println(list)

    //####################################################################################################################

    // Example 2: - Adding Custom Object to ArrayList
    // ----------------------------------------------

    /*
    // creating array list object
    var list = new ArrayList[Employee]();

    // employee object 1
    var emp1 = new Employee(101, "Dinesh");

    // employee object 2
    var emp2 = new Employee(102, "Krishnan");

    // employee object 3
    var emp3 = new Employee(103, "John");

    // adding the employee objects
    list.add(emp1);
    list.add(emp2);
    list.add(emp3);

    // looping through list object
    for (a <- 0 to (list.size() - 1)) {

      /// printing object from list
      println(list.get(a));

    }


    class Employee(empId: Int, empName: String) {

      def getEmpId() : Int = {

        return empId;
      }

      def getEmpName() : String = {

        return empName;
      }

      override def toString() : String = {

        return "[empId : "+empId+", empName : "+empName+"]";
      }

    }

    Output:
    -------
    [empId : 101, empName : Dinesh]
    [empId : 102, empName : Krishnan]
    [empId : 103, empName : John]

    */

  }

}
