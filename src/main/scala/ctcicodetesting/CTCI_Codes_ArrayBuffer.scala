package ctcicodetesting

// Scala program to create an ArrayBuffer
// ArrayBuffer class is imported
import scala.collection.mutable.ArrayBuffer

object CTCI_Codes_ArrayBuffer {

  def main(args: Array[String]): Unit =
  {

    /*
    ArrayBuffer:
    ------------
    Array in scala is homogeneous and mutable, i.e it contains elements of the same data type and its elements can
    change but the size of array size can’t change.

    To create a mutable, indexed sequence whose size can change ArrayBuffer class is used.
    To use, ArrayBuffer, scala.collection.mutable.ArrayBuffer class is imported, an instance of ArrayBuffer is created.
    Internally, an ArrayBuffer is an Array of elements, as well as the store’s current size of the array.
    When an element is added to an ArrayBuffer, this size is checked.
    If the underlying array isn’t full, then the element is directly added to the array.
    If the underlying array is full, then a larger array is constructed and all the elements are copied to the new array.
    The key is that the new array is constructed larger than required for the current addition.

    Example:
        var name = new ArrayBuffer[datatype]()  // empty buffer is created
        var name = new ArrayBuffer("chandan", "gfg", "geeksforgeeks")

    In above example, first, an empty buffer is created here datatype indicates the type of data such as integer, string.
    Then created buffer with three elements, of type string.
    */

    // Instance of ArrayBuffer is created
    var name = ArrayBuffer[String]()
    name += "GeeksForGeeks"
    name += "gfg"
    name += "Chandan"
    println(name)

    println(name(1))  // accessing 1th index element of arraybuffer

    //####################################################################################################################

    // Adding elements in ArrayBuffer:
    // -------------------------------

    // Add single element to the buffer                              ==>     ArrayBuffer+=( element)
    // Add two or more elements (method has a varargs parameter)     ==>     ArrayBuffer+= (element1, element2, ..., elementN )
    // Append one or more elements (uses a varargs parameter)        ==>     ArrayBuffer.append( elem1, elem2, ... elemN)

    // adding one element
    name += "GeeksForGeeks"

    // add two or more elements
    name += ("gfg", "chandan")

    // adding one or more element using append method
    name.append("S-series", "Ritesh")
    println(name)

    //####################################################################################################################

    // Deleting ArrayBuffer Elements:
    // ------------------------------

    // Remove one element           ==>       ArrayBuffer-= (element)
    //Remove multiple elements      ==>       ArrayBuffer-= (elem1, elem2, ....., elemN)

    // deletes one element
    name -= "GeeksForGeeks"         // ==> Check this behaviour..only one GeeksForGeeks is removed

    // deletes two or more elements
    name -= ("gfg", "Ritesh")       // ==> Check this behaviour..only one gfg is removed

    // printing resultant arraybuffer
    println(name)

    //####################################################################################################################

    // Deleting ArrayBuffer Elements:
    // ------------------------------

    // Deleting ArrayBuffer Elements using ArrayBuffer.remove() :
    // The remove method is used to delete one element by its position in the ArrayBuffer,
    // or a series of elements beginning at a starting position.

    var name2 = ArrayBuffer( "GeeksForGeeks", "gfg", "chandan", "S-series", "Ritesh" )

    // removing 0th index element
    name2.remove(0)

    // printing resultant arraybuffer
    println(name2)
    name2.remove(1, 3)

    // printing resultant arraybuffer
    println(name2)
  }

}
