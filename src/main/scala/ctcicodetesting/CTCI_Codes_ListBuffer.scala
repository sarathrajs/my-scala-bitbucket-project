package ctcicodetesting

import scala.collection.mutable.ListBuffer

// Scala program to create a ListBuffer
// ListBuffer class is imported

object CTCI_Codes_ListBuffer {

  def main (args: Array[String]) : Unit = {

    // “How do I add elements to a List?” is a bit of a trick question, because a List is immutable, so you can’t actually add elements to it.
    // If you want a List that is constantly changing, use a ListBuffer (as described in Recipe 11.2), and then convert it to a List when necessary.

    /*
  Scala ListBuffer:
  -----------------
  A list is a collection which contains immutable data. List represents linked list in Scala.
  A List is immutable, if we need to create a list that is constantly changing, the preferred approach is to use a ListBuffer.

  The Scala List class holds a sequenced, linear list of items.
  A List can be built up efficiently only from back to front.
  the ListBuffer object is convenient when we want to build a list from front to back. It supports efficient prepend and append operations.

  Once we are done creating our list, call the toList method.
  To convert the ListBuffer into a List, Time taken will be constant.
  To use ListBuffer, scala.collection.mutable.ListBuffer class is imported, an instance of ListBuffer is created.

      var name = new ListBuffer[datatype]()  // empty buffer is created
      var name = new ListBuffer("class", "gfg", "geeksforgeeks")

  */

    // Instance of ListBuffer is created
    var name = ListBuffer[String]()
    name += "GeeksForGeeks"
    name += "gfg"
    name += "Class"
    println(name) // ListBuffer(GeeksForGeeks, gfg, Class)

    //####################################################################################################################

    // Access element from ListBuffer ==> Element is accessed same as list, ListBuffer(i) is used to accessed ith index element of list.

    println(name(1)) //Accessing 1th index element of listBuffer ==> gfg

    //####################################################################################################################

    // Adding elements in ListBuffer:
    // ------------------------------

    // Add single element to the buffer                             ==>   ListBuffer+=( element)
    // Add two or more elements (method has a varargs parameter)    ==>   ListBuffer+= (element1, element2, ..., elementN )
    // Append one or more elements (uses a varargs parameter)       ==>   ListBuffer.append( elem1, elem2, ... elemN)

    // Instance of ListBuffer is created
    var name2 = ListBuffer[String]()

    // Adding one element
    name2 += "GeeksForGeeks"

    // Add two or more elements
    name2 += ("gfg", "class")

    // Adding one or more element using append method
    name2.append("Scala", "Article")

    // Printing ListBuffer
    println(name2)

    //####################################################################################################################

    // Deleting elements in ListBuffer:
    // ------------------------------

    // Remove one element         ==>     ListBuffer-= (element)
    // Remove multiple elements   ==>     ListBuffer-= (elem1, elem2, ....., elemN)

    // Instance of ListBuffer is created
    var name3 = ListBuffer(
      "GeeksForGeeks",
      "gfg",
      "class",
      "Scala",
      "Article"
    )

    // Deletes one element
    name3 -= "Article"

    // Deletes two or more elements
    name3 -= ("gfg", "class")

    // Printing resultant ListBuffer
    println("name3 is :" + name3)

    //####################################################################################################################

    // Deleting elements in ListBuffer using ListBuffer.remove():
    // ----------------------------------------------------------

    // The remove() method is used to delete one element by its position in the ListBuffer,
    // or a series of elements beginning at a starting position.

    // Instance of ListBuffer is created
    var name4 = ListBuffer( "GeeksForGeeks",
      "gfg", "class",
      "Scala", "Article" )
    println("name4 is :" + name4)

    // Removing 0th index element
    name4.remove(0)

    // Printing resultant ListBuffer
    println(name4)
    name4.remove(1, 3)

    // Printing resultant ListBuffer
    println(name4)

    //####################################################################################################################

    // Change to List:
    // ---------------

    name3.toList
    println(name3) // ListBuffer(GeeksForGeeks, Scala)
    var temp = name3.toList
    println(temp) // List(GeeksForGeeks, Scala)

  }
}
