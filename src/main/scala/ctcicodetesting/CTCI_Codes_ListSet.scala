package ctcicodetesting

import scala.collection.immutable._

object CTCI_Codes_ListSet extends App {

  /*
  ListSet in Scala:
  -----------------

  A set is a collection which only contains unique items which are not repeatable and a list is a collection which contains immutable data.
  In scala, ListSet class implements immutable sets using a list-based data structure.
  Elements are stored in reversed insertion order, That means the newest element is at the head of the list.
  It maintains insertion order.

  Listset is used only for a small number of elements.
  We can create empty ListSet either by calling the constructor or by applying the function ListSet.empty.
  It’s iterate and traversal methods visit elements in the same order in which they were first inserted.
   */

  println("Initializing an immutable ListSet ")

  // Creating ListSet
  val listSet1: ListSet[String] = ListSet("GeeksForGeeks","Article", "Scala")

  println(s"Elements of listSet1 = $listSet1")
  println("Elements of listSet1 = " + listSet1)

  //####################################################################################################################

  // Check specific elements in ListSet :
  //-------------------------------------

  println("Check elements of immutable ListSet")

  // Checking element in a ListSet
  println("GeeksForGeeks = " + listSet1("GeeksForGeeks"))
  println("Student = " + listSet1("Student"))
  println("Scala = " + listSet1("Scala"))

  //####################################################################################################################

  // Adding an elements in ListSet :
  //--------------------------------

  println("Add element of immutable ListSet ")
  val listSet2: ListSet[String] = listSet1 + "Java"
  println(s"Adding element java to ListSet $listSet2")

  //####################################################################################################################

  // Adding two ListSet :
  //---------------------

  // Adding two ListSet
  val listSet3: ListSet[String] = listSet1 ++ ListSet("Java", "Csharp")

  println(s"After adding two lists $listSet3")

  //####################################################################################################################

  // Remove element from the ListSet :
  //----------------------------------

  // We can remove an element in ListSet by using – operator. below is the example of removing an element in ListSet.

  println("Remove element from the ListSet ")

  val listSet4: ListSet[String] = listSet1 - "Article"

  println(s"After removing element from listset = $listSet4")

  //####################################################################################################################

  // Initialize an empty ListSet :
  //------------------------------

  // Creating an empty ListSet

  println("Initialize an empty ListSet")

  val emptyListSet: ListSet[String] = ListSet.empty[String]

  println(s"String type empty ListSet = $emptyListSet")

}
