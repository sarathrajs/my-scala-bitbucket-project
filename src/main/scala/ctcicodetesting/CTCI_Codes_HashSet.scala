package ctcicodetesting

// Scala program of Initializing HashSet
import scala.collection.immutable.HashSet

object CTCI_Codes_HashSet {

  def main(args:Array[String]) : Unit = {

    /*
    HashSet In Scala:
    =================
    HashSet is sealed class. It extends immutable Set and AbstractSet trait.
    Hash code is used to store elements.
    It neither sorts the elements nor maintains insertion order .
    The Set interface implemented by the HashSet class, backed by a hash table .
    In Scala, A concrete implementation of Set semantics is known HashSet.

        Syntax:
        var HashsetName = HashSet(element1, element2, element3, ....)
    */
    //####################################################################################################################

    // Operations perform with HashSet:
    //---------------------------------

    // 1. Initialize a HashSet :
    // -------------------------
    // Below is the example to create or initialize HashSet

    println("Initialize a HashSet")

    // Creating HashSet
    val hashSet: HashSet[String] = HashSet("Geeks", "GeeksForGeeks", "Author")
    println(s"Elements are = $hashSet")

    // 2. Check specific elements in HashSet :
    // ---------------------------------------

    // Checking
    println(s"Element Geeks = ${hashSet("Geeks")}")
    println(s"Element Student = ${hashSet("Student")}")

    // 3. Adding an elements in HashSet :
    // ----------------------------------
    // We can add an element in HashSet by using + sign. below is the example of adding an element in HashSet.

    // Adding an element in HashSet
    val hs1: HashSet[String] = hashSet + "GeeksClasses"
    println(s"Adding elements to HashSet = $hs1")

    // 4. Adding more than one element in HashSet:
    // -------------------------------------------
    // We can add more than one element in HashSet by using ++ sign. below is the example of adding more than one elements in HashSet.

    // Adding elements in HashSet
    val hs2: HashSet[String] = hashSet ++ HashSet[String]("Java", "Scala")
    println(s"Add more than one HashSets = $hs2")

    // 5. Remove element in HashSet:
    // -----------------------------
    // We can remove an element in HashSet by using – sign. below is the example of removing an element in HashSet.

    // removing elements in HashSet
    val hs3: HashSet[String] = hs2 - "Geeks"
    println(s"remove element from hashset = $hs3")

    // 6. Find the intersection between two HashSets:
    // ----------------------------------------------
    // We can find intersection between two HashSets by using & sign. below is the example of finding intersection between two HashSets.

    println("Initialize two HashSets")

    // Creating two HashSet
    val hs4: HashSet[String] = HashSet("Geeks", "GeeksForGeeks", "Author")
    println(s"Elements of hashset4 are = $hs4")

    val hs5: HashSet[String] = HashSet("Java", "Geeks", "Scala")
    println(s"Elements of hashset5 are = $hs5")

    // finding the intersection between two HashSets
    println(s"Intersection of hashSet1 and hashSet2 = ${hs4 & hs5}")

    // 7. Initializing an empty HashSet:
    // ---------------------------------

    // Initializing an empty HashSet
    val emptyHashSet: HashSet[String] = HashSet.empty[String]
    println(s"Empty HashSet = $emptyHashSet")


  }

}
