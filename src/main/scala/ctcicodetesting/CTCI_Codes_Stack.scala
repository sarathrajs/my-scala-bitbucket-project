package ctcicodetesting

object CTCI_Codes_Stack {

  def main (args : Array[String]): Unit = {

    /*
    Stack in Scala:
    ---------------
    A stack is a data structure that follows the last-in, first-out(LIFO) principle.
    We can add or remove element only from one end called top.
    Scala has both mutable and immutable versions of a stack.

    Syntax :

        import scala.collection.mutable.Stack
        var s = Stack[type]()

        // OR
        var s = Stack(val1, val2, val3, ...)
    */

    //####################################################################################################################

    // Basic Operations on Stack:
    // ==========================

    // Once stack has been created we can either push elements to the stack or pop them out of the stack.

    // 1. Push operation:
    // ------------------
    // We can push element of any type to the stack using push() function. All elements must have same data type.

    // Scala program to
    // push element
    // to the stack

    import scala.collection.mutable.Stack

    var s = Stack[Int]()

    // pushing values
    // one at a time
    s.push(5)
    s.push(1)
    s.push(2)
    println("s:" + s)

    var s2 = Stack[Int]()

    // pushing multiple values
    s2.push(5,1,2)
    println("s2:" + s2)

    // 2. Pop operation:
    // -----------------
    // We can pop element from top of the stack using pop function. The function returns the same type as that of elements of the stack.

    println("Popped:" + s.pop)
    println("Popped:" + s.pop)
    println("Popped:" + s.pop)

    //####################################################################################################################

    // Other Functions:
    // ================

    // 1. isEmpty: To check whether the stack is empty. Returns true if it is empty.
    // -----------------------------------------------------------------------------
    println("Popped:" + s2.pop)
    println("Popped:" + s2.pop)
    println("Empty:" + s2.isEmpty)
    println("Popped:" + s2.pop)

    // all three elements popped
    println("Empty:" + s2.isEmpty)

    // 2. top: Returns the element that is currently at the top of the stack.
    // ----------------------------------------------------------------------
    var s3 = Stack[Int]()

    s3.push(5)
    s3.push(1)
    s3.push(2)
    println(s3)
    println("Top: " + s3.top)
    println("Popped:" + s3.pop)
    println("Top: " + s3.top)

    // 3. size:Returns the number of elements present in the stack.
    // -------------------------------------------------------------

    println("Size: " + s3.size)

  }

}
