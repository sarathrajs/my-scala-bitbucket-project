package ctcicodetesting

// https://www.geeksforgeeks.org/scala-programming-language/
// Scala program to print Hello, Geeks!
// by using object-oriented approach

// creating object

object Scala_Intro {

  def main(args:Array[String]) : Unit = {

    println("Intro to Scala!!")

  }
}

/* INTRO :
==========
Scala is a general-purpose, high-level, multi-paradigm programming language.
It is a pure object-oriented programming language which also provides the support to the functional programming approach.
There is no concept of primitive data as everything is an object in Scala.
It is designed to express the general programming patterns in a refined, succinct, and type-safe way.
Scala programs can convert to bytecodes and can run on the JVM(Java Virtual Machine).
Scala stands for Scalable language.
It also provides the Javascript runtimes.
Scala is highly influenced by Java and some other programming langauges like Lisp, Haskell, Pizza etc.

Why Scala?:
===========
Scala has many reasons for being popular among programmers. Few of the reasons are :

Easy to Start: Scala is a high level language so it is closer to other popular programming languages like Java, C, C++.
Thus it becomes very easy to learn Scala for anyone. For Java programmers, Scala is more easy to learn.

Contains best Features: Scala contains the features of different languages like C, C++, Java etc.
which makes the it more useful, scalable and productive.

Close integration with Java: The source code of the Scala is designed in such a way that its compiler can interpret the Java classes.
Also, Its compiler can utilize the frameworks, Java Libraries, and tools etc. After compilation, Scala programs can run on JVM.

Web – Based & Desktop Application Development: For the web applications it provides the support by compiling to JavaScript.
Similarly for desktop applications, it can be compiled to JVM bytecode.

Used by Big Companies: Most of the popular companies like Apple, Twitter, Walmart, Google etc. move their most of codes to Scala from some other languages.
reason being it is highly scalable and can be used in backend operations.

Note: People always thinks that Scala is a extension of Java. But it is not true. It is just completely interoperable with Java.
Scala programs get converted into .class file which contains Java Byte Code after the successful compilation and then can run on JVM(Java Virtual Machine).

Beginning with Scala Programming:
=================================
Finding a Compiler: There are various online IDEs such as GeeksforGeeks IDE, Scala Fiddle IDE etc.
which can be used to run Scala programs without installing.

Programming in Scala: Since the Scala is a lot similar to other widely used languages syntactically, it is easier to code and learn in Scala.
Programs can be written in Scala in any of the widely used text editors like Notepad++, gedit etc. or on any of the text-editors.
After writing the program, save the file with the extension .sc or .scala.

For Windows & Linux: Before installing the Scala on Windows or Linux, you must have Java Development Kit(JDK) 1.8 or greater installed on your system.
Because Scala always runs on Java 1.8 or above.

Features of Scala:
==================
There are many features which makes it different from other languages.

Object- Oriented: Every value in Scala is an object so it is a purely object-oriented programming language.
The behavior and type of objects are depicted by the classes and traits in Scala.

Functional: It is also a functional programming language as every function is a value and every value is an object.
It provides the support for the high-order functions, nested functions, anonymous functions etc.

Statically Typed: The process of verifying and enforcing the constraints of types is done at compile time in Scala.
Unlike other statically typed programming languages like C++, C etc., Scala doesn’t expect the redundant type information from the user.
In most cases, the user has no need to specify a type.

Extensible: New language constructs can be added to Scala in form of libraries.
Scala is designed to interpolate with the JRE(Java Runtime Environment).

Concurrent & Synchronize Processing: Scala allows the user to write the codes in an immutable manner that makes it easy
to apply the parallelism(Synchronize) and concurrency.

Run on JVM & Can Execute Java Code: Java and Scala have a common runtime environment. So the user can easily move from Java to Scala.
The Scala compiler compiles the program into .class file, containing the Bytecode that can be executed by JVM.
All the classes of Java SDK can be used by Scala. With the help of Scala user can customize the Java classes.

Advantages:
===========
* Scala’s complex features provided the better coding and efficiency in performance.
* Tuples, macros, and functions are the advancements in Scala.
* It incorporates the object-oriented and functional programming which in turn make it a powerful language.
* It is highly scalable and thus provides a better support for backend operations.
* It reduces the risk associated with the thread-safety which is higher in Java.
* Due to the functional approach, generally, a user ends up with fewer lines of codes and bugs which result in higher productivity and quality.
* Due to lazy computation, Scala computes the expressions only when they are required in the program.
* There are no static methods and variables in Scala. It uses the singleton object(class with one object in the source file).
* It also provides the Traits concept. Traits are the collection of abstract and non-abstract methods which can be compiled into Java interfaces.

Disadvantages:
==============
* Sometimes, two approaches make the Scala hard to understand.
* There is a limited number of Scala developers available in comparison to Java developers.
* It has no true-tail recursive optimization as it runs on JVM.
* It always revolves around the object-oriented concept because every function is value and every value is an object in Scala.

Applications:
==============
* It is mostly used in data analysis with the spark.
* Used to develop the web-applications and API.
* It provide the facility to develop the frameworks and libraries.
* Preferred to use in backend operations to improve the productivity of developers.
* Parallel batch processing can be done using Scala.

Setting up the environment in Scala:
====================================
https://www.geeksforgeeks.org/setting-up-the-environment-in-scala/

How to run a Scala Program?:
=============================
* To use an online Scala compiler:
    We can use various online IDE. which can be used to run Scala programs without installing.

* Using Command-Line:
    Make sure we have the Java 8 JDK (also known as 1.8). run javac -version in the command line and make sure we see javac 1.8.___
    If we don’t have version 1.8 or higher, Install the JDK Firstly, open a text editor Notepad or Notepad++.
    write the code in the text editor and save the file with (.scala) extension.
    open the command prompt follow step by step process on your system.

    // Scala program to print Hello World!
    object Geeks
    {
        // Main Method
        def main(args: Array[String])
        {
            // prints Hello World
            println("Hello World!")
        }
    }

Step 1: Compile above file using scalac Hello.Scala after compilation it will generate a Geeks.class file
        and class file name is same as Object name(Here Object name is Geeks).

          scalac Hello.Scala

Step 2: Now open the command with object name scala Geeks. It will give the result.

          scala Geeks

Working Example: Intro.scala
================
  object FirstProgram {

    def main (args:Array[String]) : Unit = {

      println("First Scala Program")

    }
  }

          /*
          sarath (master #) scala_code_for_datastructure $ ls -ltr
          total 8
          -rw-r--r--  1 sarath  staff  109 Feb 13 10:08 Intro.scala

          sarath (master #) scala_code_for_datastructure $ scalac Intro.scala

          sarath (master #) scala_code_for_datastructure $ ls -ltr
          total 24
          -rw-r--r--  1 sarath  staff  109 Feb 13 10:08 Intro.scala
          -rw-r--r--  1 sarath  staff  688 Feb 13 10:08 FirstProgram$.class
          -rw-r--r--  1 sarath  staff  651 Feb 13 10:08 FirstProgram.class

          sarath (master #) scala_code_for_datastructure $ scala FirstProgram
          First Scala Program
          */
*/