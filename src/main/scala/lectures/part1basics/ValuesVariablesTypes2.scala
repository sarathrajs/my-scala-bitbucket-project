package lectures.part1basics

object ValuesVariablesTypes2 extends App {

  // Keywords or Reserved words are the words in a language that are used for some internal process or represent some predefined actions.
  // These words are therefore not allowed to use as variable names or objects. Doing this will result in a compile-time error.
  // Here object, def, and var are valid keywords

  // Variable Declaration:
  //----------------------

  /*
  Rules for naming variable in Scala:

    Variable name should be in lower case.
    Variable name can contain letter, digit and two special characters(Underscore(_) and Dollar($) sign)
    Variable name must not contain the keyword or reserved word.
    Starting letter of the variable name should be an alphabet.
    White space is not allowed in variable name.
    Note: Scala supports multiple assignments, but you can use multiple assignments only with immutable variables.

    For Example: val(name1:Int, name2:String) = pair(2, "geekforgeeks")
  */


  val value_one: String = "Sarath"
  val value_two = "Preethi" // Here compiler can infer the type as string.. no need to mention it explicitly.
  println(value_one)
  println(value_two)

  val num1 = 10
  println(num1)
  // num1 = 20 ------> This is not allowed..since a is a val type..so reassignment cannot be done.
  // VALS (values) ARE IMMUTABLE..Hence comes VAR (variables)
  val num2 = 15
  println(num1 + num2)
  println(num1 == num2)

  var variable1 = 10
  println(variable1)
  variable1 = 20 // this is a side effect..we will see the dangers of side effects later.
  variable1 += 1
  println(variable1)

  val bool1: Boolean = true
  val bool2: Boolean = false
  println(bool1 + "..." + bool2)

  val char1: Char = 'a'
  val char2: Char = 'b'
  val anInt: Int = variable1
  val aShort: Short = 123
  val aLong: Long = 12345678908864728L // If we don't mention the L..it will show an error.
  val bLong: Long = 123456789
  val aFloat: Float = 2.0f // Just 2.0 will throw you an error saying that 2.0 is a double not a float.
  val aDouble: Double = 2.0
  val aDecimal: BigDecimal = 334354545454545454.1434353545 // Check this for more info.

  // New data:
  println("------ New Data ------")
  var a: Boolean = true
  var a1: Byte = 126
  var a2: Float = 2.45673f
  var a3: Int = 3
  var a4: Short = 45
  var a5: Double = 2.93846523
  var a6: Char = 'A'
  if (a == true)
  {
    println("boolean:geeksforgeeks")
  }
  println("byte:" + a1)
  println("float:" + a2)
  println("integer:" + a3)
  println("short:" + a4)
  println("double:" + a5)
  println("char:" + a6)

  /*
  Scala contains following keywords:
  ==================================
  abstract
  case
  catch
  class
  def
  do
  else
  extends
  false
  final
  finally
  for
  forSome
  if
  implicit
  import
  lazy
  match
  new
      null
  object
  override
  package
  private
  protected
  return
  sealed
  super
  this
  throw
  trait
  true
  try
    type
    val
    var
    while
  with
  yield
  >:
  ⇒
  =>
  =
  <%
  <:
  ←
  <-
  #
  @
  :

  Scala Identifiers:
  ==================
  In programming languages, Identifiers are used for identification purpose.
  In Scala, an identifier can be a class name, method name, variable name or an object name.

    class GFG{
      var a: Int = 20
    }

    object Main {
      def main(args: Array[String]) {
        var ob = new GFG();
      }
    }

   In the above program we have 6 identifiers:
      GFG: Class name
      a: Variable name
      Main: Object name
      main: Method name
      args: Variable name
      ob: Object name

   Rules for defining Java Scala:
   ==============================
   There are certain rules for defining a valid Scala identifier. These rules must be followed, otherwise we get a compile-time error.

      * Scala identifiers are case-sensitive.
      * Scala does not allows you to use keyword as an identifier.
      * Reserved Words can’t be used as an identifier like $ etc.
      * Scala only allowed those identifiers which are created using below four types of identifiers.
      * There is no limit on the length of the identifier, but it is advisable to use an optimum length of 4 – 15 letters only.
      * Identifiers should not start with digits([0-9]). For example “123geeks” is a not a valid Scala identifier.

   Example of valid alphanumeric identifiers: _GFG, geeks123, _1_Gee_23, Geeks
   Example of Invalid alphanumeric identifiers: 123G, $Geeks, -geeks

*/

  /*
  Literals in Scala : Here we will discuss different types of literals used in Scala.
  ===================================================================================

  Integral Literal: These are generally of int type or long type (“L” or “I” suffix used ). Some legal integral literals are:
      02
      0
      40
      213
      0xFFFFFFFF
      0743L

  Floating-point Literals: These are of float type(“f” or”F” suffix used ) and of double type.
      0.7
      1e60f
      3.12154f
      1.0e100
      .3

  Boolean Literals: These are of Boolean type and it contains only true and false.

  Symbol Literals: In Scala, symbol is a case class. In symbol literal, a’Y’ is identical to scala.Symbol(“Y”).
      package scala
      final case class Symbol private (name: String) {
            override def toString: String = “‘” + name
      }

  Character Literals: In Scala, character literal is a single character that is encircled between single quotes.
  There characters are printable unicode character and also described by the escape character. Few valid literals are shown below:
      ‘\b’
      ‘a’
      ‘\r’
      ‘\u0027’

  String Literals: In Scala, string literals are the sequence of character that are enclosed between double quotes.
  Some valid literals as shown below:
       “welcome to \n geeksforgeeks”
       “\\This is the tutorial of Scala\\”

  Null Values: In Scala, null value is of scale.Null type, that’s the way it is adaptable with every reference type.
  It is indicated as a reference value which refers to a special “null” object.

  Multi-line Literals: In Scala, multi-line literals are the sequence of characters that are encircled in between triple quotes.
  In this new line and other control characters are valid. Some valid multi-line literals shown below:
      “””welcome to geeksforgeeks\n
      this is the tutorial of \n
      scala programing language”””
   */

}
