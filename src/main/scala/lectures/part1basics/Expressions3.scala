package lectures.part1basics

import scala.util.control.Breaks._

object Expressions3 extends App {

  val x = 1 + 3 //Expressions
  println(x)
  println(10 + 20 * 2)

  //Math Operators in Scala:
  // + , - , * , / ... Math Operators.
  // & | ^  << >>  ... Bitwise Operators.
  // >>> (right shift with zero extension .. only specific to scala)

  //Relational Operators in Scala:
  // == : Equality operator
  // != : Non Equality operator
  // >= > < <=
  println(1 == x)

  //Boolean Operators in Scala:
  // ! : Logical Negation unary operator
  // &&
  // ||
  println(!(1 == x))

  var aNumber = 10
  aNumber += 1
  println(aNumber)
  aNumber -= 2
  println(aNumber)
  aNumber *= 3
  println(aNumber)
  aNumber /= 9
  println(aNumber)

  //VERY IMPORTANT TO UNDERSTAND:
  //INSTRUCTIONS (what we tell a computer to do) VS EXPRESSIONS (value or type)
  // i.e., instructions are executed and expressions are evaluated.

  // Lets see above with an example IF Expression:
  val aCondition = true
  val anotherValue = if (aCondition) 5000 else 3000 // Here we can see that this IF is an Expression
  println(anotherValue)
  println(if (aCondition) 6000 else 4000)

  //NEXT LOOPS in SCALA... WE SHOULD NEVER USE LOOPS IN SCALA...EVER !!!
  //Example of loop:
  var i = 0
  while (i < 10) {
    println(i)
    i += 1
  } // Never Ever use the above..since they execute side effects.
  // While and Loops are very specific to imperative programming that is java.
  // Single worst thing a scala programmer can do really is write imperative code.

  //Everything in scala is an EXPRESSION.

  //Lets see a special type here called "unit"..which is equivalent to void.
  // Example:
  val aWeirdValue = (aNumber = 3)
  println(aWeirdValue) // Here the printed output will be () // This is a unit // aNumber = 3 does not return anything

  // Very important : Side Effects in Scala are actually expressions returning unit.
  // Examples of Side Effects are:
  //      * println()
  //      * whiles
  //      * reassigning of vars

  // We might think "aNumber = 3" is an instruction..but actually they are expressions returning unit.

  //--------------------------------------------------------------------------------------------------------------------

  // CODE BLOCKS:
  //-------------

  val aCodeBlock = {

    val num1 = 10
    val num2 = 20

    if (num2 > num1)
      "hello"
    else
      "goodbye"
  }

  println(aCodeBlock)

  //--------------------------------------------------------------------------------------------------------------------

  //Takeaway:
  //---------

  val aa = 3 + 5
  val xIsEven = aa % 2 == 0
  val xIsOdd = !xIsEven

  println(aa)
  println(xIsEven)
  println(xIsOdd)

  // 1. difference between "hello world" vs println("hello world")?
  // First is String literal and second is an expression

  // Whats the value of these below??

  val someValue = {
    2 < 3
  }
  println(someValue)

  val someOtherValue1 = {
    if (someValue) 239 else 986
  }
  println(someOtherValue1)

  val someOtherValue2 = {
    if (someValue) 239 else 986
    444
  }
  println(someOtherValue2)

  //--------------------------------------------------------------------------------------------------------------------

  // Here, breakable is used to prevent exception
  breakable
  {
    for (a <- 1 to 10)
    {
      if (a == 6)

      // terminate the loop when
      // the value of a is equal to 6
        break
      else
        println(a)
    }
  }

}
