package lectures.part1basics

/* #####################################################################################################

This will not produce a Run command when you right click the object..since there is no main function:
--------------------------------------------------------------------------------------------------------
object scalaintro {
    println("Scala Intro")
}

This is the correct way to do it: (now we will get a run when we right click this object)
--------------------------------------------------------------------------------------------
object scalaintro {
  def main(args: Array[String]): Unit = {
    println("Scala Intro")
  }
}

We can also use an alternative and use "extends App" function which will automatically insert a main function for this object:
------------------------------------------------------------------------------------------------------------------------------
*/

object scalaintro1 extends App {

  println("Scala Intro !! ")

}


/*
  1. What is Scala
  ----------------
    Scala is an acronym for “Scalable Language”. It is a general-purpose programming language designed for the programmers who want
    to write programs in a concise, elegant, and type-safe way. Scala enables programmers to be more productive.
    Scala is developed as an object-oriented and functional programming language.

    If you write a code in Scala, you will see that the style is similar to a scripting language.
    Even though Scala is a new language, it has gained enough users and has a wide community support.
    It is one of the most user-friendly languages.

  2.1 Scala is pure Object-Oriented programming language
  ------------------------------------------------------

    Scala is an object-oriented programming language. Everything in Scala is an object and any operations you perform is a method call.
    Scala, allow you to add new operations to existing classes with the help of implicit classes.

    One of the advantages of Scala is that it makes it very easy to interact with Java code. You can also write a Java code inside Scala class.
    The Scala supports advanced component architectures through classes and traits.

  2.2 Scala is a functional language
  ----------------------------------

    Scala is a programming language that has implemented major functional programming concepts.
    In Functional programming, every computation is treated as a mathematical function which avoids states and mutable data.
    The functional programming exhibits following characteristics:

        Power and flexibility
        Simplicity
        Suitable for parallel processing
        Scala is not a pure functional language. Haskell is an example of a pure functional language.

   2.3 Scala is a compiler based language (and not interpreted)
   ------------------------------------------------------------

    Scala is a compiler based language which makes Scala execution very fast if you compare it with Python (which is an interpreted language).
    The compiler in Scala works in similar fashion as Java compiler. It gets the source code and generates Java byte-code that can be executed independently
    on any standard JVM (Java Virtual Machine).

    There are more important points about Scala which I have not covered. Some of them are:

      * Scala has thread based executors
      * Scala is statically typed language
      * Scala can execute Java code
      * You can do concurrent and Synchronized processing in Scala
      * Scala is JVM based languages

    6. Scala Basics Terms
    ---------------------
      * Object: An entity that has state and behavior is known as an object. For example: table, person, car etc.
      * Class: A class can be defined as a blueprint or a template for creating different objects which defines its properties and behavior.
      * Method: It is a behavior of a class. A class can contain one or more than one method. For example: deposit can be considered a method of bank class.
      * Closure: Closure is any function that closes over the environment in which it’s defined.
                 A closure returns value depends on the value of one or more variables which is declared outside this closure.
      * Traits: Traits are used to define object types by specifying the signature of the supported methods. It is like interface in java.

    7. Things to note about Scala
    -----------------------------
      * It is case sensitive
      * If you are writing a program in Scala, you should save this program using “.scala”
      * Scala execution starts from main() methods
      * Any identifier name cannot begin with numbers. For example, variable name “123salary” is invalid.
      * You can not use Scala reserved keywords for variable declarations or constant or any identifiers.

    8. Variable declaration in Scala
    --------------------------------
      *In Scala, you can declare a variable using ‘var’ or ‘val’ keyword. The decision is based on whether it is a constant or a variable.
      * If you use ‘var’ keyword, you define a variable as mutable variable. On the other hand, if you use ‘val’, you define it as immutable.
      * Let’s first declare a variable using “var” and then using “val”.
*/
