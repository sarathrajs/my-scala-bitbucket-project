package lectures.part1basics

class GFG
{
  var name = "Priyanka"
  var age = 20
  var branch = "Computer Science"
  def show()
  {
    println("Hello! my name is " + name + "and my age is"+ age)
    println("My branch name is " + branch)
  }
}

object Classes5 {

  def main(args: Array[String])
  {
    var ob = new GFG()
    ob.show()
  }

}
