package lectures.part1basics

object Functions4 extends App {

  println("Functions")

  def function1(a: String, b: Int): String = {
    a + " " + b
  }

  // Since above code block has only one line...we can omit the {}

  def function2(a: String, b: Int): String = a + " " + b

  def function3(a: String, b: Int) = a + " " + b

  val result1 = function1("Sarath", 11)
  println(result1)

  val result2 = function2("Raj", 11)
  println(result2)

  println(function1("S", 10))

  def aParameterLessFunction(): Int = 42

  println(aParameterLessFunction()) // with the () at the end.
  println(aParameterLessFunction) // we are not using the () at the end.

  //--------------------------------------------------------------------------------------------------------------------

  // We are going to replace the Loops we saw in previous lecture with a function:
  // Example function for recursive behaviour

  def aRepeatedBehaviour(name: String, times: Int): String = {
    if (times == 1) name
    else
      name + " " + aRepeatedBehaviour(name, times - 1)
  }

  println(aRepeatedBehaviour("Sachin", 4))

  //above we need to specify the return type of the recursive function else compiler will throw an error.
  //def aRepeatedBehaviourTemp(name: String, times: Int) = {
  //  if (times == 1) name
  //  else
  //    name + " " + aRepeatedBehaviourTemp(name, times - 1)
  //}

  // : Error : recursive method needs result type.

  //--------------------------------------------------------------------------------------------------------------------
  // Nested Functions:
  //------------------
  def aBiggerFunction(a: Int): Int = {

    def aSmallerFunction(a: Int, b: Int): Int = a + b

    aSmallerFunction(a, a + 1)
  }


  //--------------------------------------------------------------------------------------------------------------------
  //TEST:
  //-----

  //When you need Loops, use recursion..lets rewrite the loop below using recursion:
  //var i = 0
  //while (i < 10) {
  //  println(i)
  //  i += 1
  //}

  def aRepeatedBehaviour2(times: Int, initial_value: Int = 0): Unit = {
    if (initial_value < times) {
      println(initial_value)
      aRepeatedBehaviour2(times, initial_value + 1)
    }
    else {
      println("End of Loop")
    }
  }

  aRepeatedBehaviour2(10)

  /*
   1.  A greeting function (name, age) => "Hi, my name is $name and I am $age years old."
   2.  Factorial function 1 * 2 * 3 * .. * n
   3.  A Fibonacci function
       f(1) = 1
       f(2) = 1
       f(n) = f(n - 1) + f(n - 2)
   4.  Tests if a number is prime.
  */

  def aGreeterFunction(name: String, age: Int): Unit = {
    println("Hi, my name is " + name + " and I am " + age + " years old")
  }

  aGreeterFunction("Sarath", 29)

  //MY code:
  //--------

  def aFactorialFunction(num: Int, initial_value: Int = 1): Int = {
    if (initial_value <= num) initial_value * aFactorialFunction(num, initial_value + 1)
    else 1
  }

  println(aFactorialFunction(5))

  // ACTUAL VALUES:
  //---------------
  def factorial(n: Int): Int =
    if (n <= 0) 1
    else n * factorial(n - 1)

  println(factorial(5))

  //MY code:
  //--------

  def aFibonacciFunction(num: Int): Int =
    if (num == 1 || num == 2) 1
    else aFibonacciFunction(num - 1) + aFibonacciFunction(num - 2)

  println(aFibonacciFunction(1))
  println(aFibonacciFunction(2))
  println(aFibonacciFunction(6)) //f(1) = 1 / f(2) = 1 / f(3) = 2 / f(4) = 3/ f(5) = 5 / f(6)=8

  // ACTUAL VALUES:
  //---------------
  def fibonacci(n: Int): Int =
    if (n <= 2) 1
    else fibonacci(n - 1) + fibonacci(n - 2)

  // 1 1 2 3 5 8 13 21
  println(fibonacci(8))

  //MY code:(incomplete / incorrect )
  //--------
  def aPrimeFunction(num: Int): Boolean = {

    def findIfDivisible(num2: Int): Boolean =
      if (num % num2 == 0) false
      else true

    val num2 = num / 2 + 1
    findIfDivisible(num2 - 1) // can be rewritten as "findIfDivisible(num / 2)"
  }

  println(aPrimeFunction(27))
  println(aPrimeFunction(11))
  println(aPrimeFunction(13))

  // ACTUAL VALUES:
  //---------------
  def isPrime(n: Int): Boolean = {

    def isPrimeUntil(t: Int): Boolean =
      if (t <= 1) true
      else n % t != 0 && isPrimeUntil(t - 1)

    isPrimeUntil(n / 2)
  }

  println(isPrime(37))
  println(isPrime(2003))
  println(isPrime(37 * 17))

  //Learn about recursive and tail recursive function... eg: line number 133 .. 160

}
