package strings_leetcode_examples

object Leetcode_strings_easy_5 {

  def main(args:Array[String]) : Unit = {

    /*
    Valid Palindrome:
    Given a string, determine if it is a palindrome, considering only alphanumeric characters and ignoring cases.
    Note: For the purpose of this problem, we define empty string as valid palindrome.

    Example 1:

    Input: "A man, a plan, a canal: Panama"
    Output: true

    Example 2:

    Input: "race a car"
    Output: false

    Constraints: s consists only of printable ASCII characters.
    */

    def isPalindrome(s: String): Boolean = {

      var start = 0
      var end = s.length -1
      var arr = s.toCharArray

      while (start < end)
        {

          var temp1 = if (arr(start) >= 97) arr(start) - 32 else arr(start)
          var temp2 = if (arr(end) >= 97) arr(end) - 32 else arr(end)
          // a 97 : z 122
          // A 65 : Z 090

          if (temp1 != temp2)
            {
              return false
            }
          start += 1
          end -= 1
        }

      println("--------------------------------")
      true
    }

    //##################################################################################################################

    //var input1 = "A man, a plan, a canal: Panama"
    var input2 = "Malayalam"

    //var result1 = isPalindrome(input1)
    var result2 = isPalindrome(input2)

    //println(result1)
    println(result2)

  }
}
