package strings_leetcode_examples

object Leetcode_strings_easy_9 {

  def main(args:Array[String]) : Unit = {

    /*
    Longest Common Prefix:

    Write a function to find the longest common prefix string amongst an array of strings.
    If there is no common prefix, return an empty string "".

    Example 1:

    Input: strs = ["flower","flow","flight"]
    Output: "fl"

    Example 2:

    Input: strs = ["dog","racecar","car"]
    Output: ""

    Explanation: There is no common prefix among the input strings.

    Constraints:
      0 <= strs.length <= 200
      0 <= strs[i].length <= 200
      strs[i] consists of only lower-case English letters.
    */

    def longestCommonPrefix(strs: Array[String]): String = {

      strs.length match {
        case 0 => ""
        case 1 => strs(0)
        case _ =>

          val len = strs.map(x => x.length).min

          for (i <- 0 until len) {

            if (strs.map(x => x(i)).distinct.length > 1) return strs(0).substring(0, i)
          }

          strs(0).substring(0, len)
      }
    }

    //##################################################################################################################

    var input = Array("flower","flow","flight")
    var result = longestCommonPrefix(input)
    println(result)
  }

}
