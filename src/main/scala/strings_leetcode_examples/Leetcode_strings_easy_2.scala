package strings_leetcode_examples

object Leetcode_strings_easy_2 {

  def main(args:Array[String]) : Unit = {

    /*
    Reverse Integer:
    Given a signed 32-bit integer x, return x with its digits reversed.
    If reversing x causes the value to go outside the signed 32-bit integer range [-231, 231 - 1], then return 0.
    Assume the environment does not allow you to store 64-bit integers (signed or unsigned).

    Example 1:

    Input: x = 123
    Output: 321

    Example 2:

    Input: x = -123
    Output: -321

    Example 3:

    Input: x = 120
    Output: 21

    Example 4:

    Input: x = 0
    Output: 0
    */

    def reverse(x: Int): Int = {

      var result = x
      var result_1 = 0
      var sign = 1

      if (result < 0)
      {
        sign = -1
        result = result * sign
      }

      while (result > 0)
        {
          result_1 = result_1 * 10 + result%10
          result = result/10
        }

      result_1*sign
    }

    //##################################################################################################################

    var input1 = 123
    var input2 = -123
    var input3 = 120
    var input4 = 0
    var result1 = reverse(input1)
    var result2 = reverse(input2)
    var result3 = reverse(input3)
    var result4 = reverse(input4)
    println("-------------------------")
    println(result1)
    println("-------------------------")
    println(result2)
    println("-------------------------")
    println(result3)
    println("-------------------------")
    println(result4)
    println("-------------------------")

  }
}
