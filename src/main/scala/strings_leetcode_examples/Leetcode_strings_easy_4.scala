package strings_leetcode_examples

import scala.collection.mutable._

object Leetcode_strings_easy_4 {

  def main(args:Array[String]) : Unit = {

    /*
    Valid Anagram:
    Given two strings s and t , write a function to determine if t is an anagram of s.

    Example 1:

    Input: s = "anagram", t = "nagaram"
    Output: true

    Example 2:

    Input: s = "rat", t = "car"
    Output: false

    Note:
    You may assume the string contains only lowercase alphabets.

    Follow up:
    What if the inputs contain unicode characters? How would you adapt your solution to such case?
    */

    def isAnagram(s: String, t: String): Boolean = {

      var my_hashmap1 = new HashMap[Char,Int]()
      var my_hashmap2 = new HashMap[Char,Int]()

      for (i <- s)
        {
          if(!my_hashmap1.contains(i)){
            my_hashmap1.put(i,1)
          }
          else{
            my_hashmap1(i) += 1
          }
        }

      for (i <- t)
      {
        if(!my_hashmap2.contains(i)){
          my_hashmap2.put(i,1)
        }
        else{
          my_hashmap2(i) += 1
        }
      }

      return (my_hashmap1 equals my_hashmap2)
    }
    //##################################################################################################################

    var input1 = "anagram"
    var input2 = "nagaram"

    var result = isAnagram(input1,input2)

    println(result)
  }
}
