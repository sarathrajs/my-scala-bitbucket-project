package strings_leetcode_examples

import scala.collection.mutable.HashMap

object Leetcode_strings_easy_3 {

  def main(args:Array[String]) : Unit = {

    /*
    First Unique Character in a String:

    Given a string, find the first non-repeating character in it and return its index. If it doesn't exist, return -1.

    Examples:

    s = "leetcode"
    return 0.

    s = "loveleetcode"
    return 2.

    Note: You may assume the string contains only lowercase English letters.
    */

    def firstUniqChar(s: String): Int = {

      var my_Hashmap = new HashMap[Char,Int]()
      var j,k = 0

      for ( i <- s)
        {
          if(!my_Hashmap.contains(i)){
            //my_Hashmap ++= HashMap( i -> 1)
            my_Hashmap.put(i,1)
          }
          else{
            my_Hashmap(i) += 1
          }
        }

      for ( i <- s )
        {
          if (my_Hashmap(i) == 1){
            return k
            // return s.indexOf(i)
          }
          k += 1
        }

      return -1
    }

    //##################################################################################################################

    var input = "loveleetcode"
    var result = firstUniqChar(input)
    println(result)

  }

}
