package strings_leetcode_examples

object Leetcode_strings_easy_7 {

  def main(args:Array[String]) : Unit = {

    /*
    Implement strStr():

    Return the index of the first occurrence of needle in haystack, or -1 if needle is not part of haystack.
    Clarification:
    What should we return when needle is an empty string? This is a great question to ask during an interview.
    For the purpose of this problem, we will return 0 when needle is an empty string.
    This is consistent to C's strstr() and Java's indexOf().

    Input: haystack = "hello", needle = "ll"
    Output: 2

    Input: haystack = "aaaaa", needle = "bba"
    Output: -1

    Input: haystack = "", needle = ""
    Output: 0
    */

    def strStr(haystack: String, needle: String): Int = {

      var needle_length = needle.length

      def recursivestrStr(haystack: String, idx: Int): Int = {

        if (haystack.length > needle_length) {

          if (haystack.substring(0, needle_length) == needle) return idx
          else recursivestrStr(haystack.tail, idx + 1)
        }
        else -1

      }

      if(needle_length == 0)
        0
      else
        recursivestrStr(haystack,0)


    }

    //##################################################################################################################

    val haystack = "hello"
    val needle = "ll"
    val result = strStr(haystack,needle)
    println(result)

  }
}
