package strings_leetcode_examples

object Leetcode_strings_easy_6 {

  def main(args:Array[String]) : Unit = {

    /*
    String to Integer (atoi):
    Implement the myAtoi(string s) function, which converts a string to a 32-bit signed integer (similar to C/C++'s atoi function).

    The algorithm for myAtoi(string s) is as follows:

    Read in and ignore any leading whitespace.
    Check if the next character (if not already at the end of the string) is '-' or '+'. Read this character in if it is either. This determines if the final result is negative or positive respectively. Assume the result is positive if neither is present.
    Read in next the characters until the next non-digit charcter or the end of the input is reached. The rest of the string is ignored.
    Convert these digits into an integer (i.e. "123" -> 123, "0032" -> 32). If no digits were read, then the integer is 0. Change the sign as necessary (from step 2).
    If the integer is out of the 32-bit signed integer range [-231, 231 - 1], then clamp the integer so that it remains in the range. Specifically, integers less than -231 should be clamped to -231, and integers greater than 231 - 1 should be clamped to 231 - 1.
    Return the integer as the final result.

    Note:
    Only the space character ' ' is considered a whitespace character.
    Do not ignore any characters other than the leading whitespace or the rest of the string after the digits.

    1. Input: s = "42"
    Output: 42

    2. Input: s = "   -42"
    Output: -42

    3. Input: s = "4193 with words"
    Output: 4193

    4. Input: s = "words and 987"
    Output: 0

    5. Input: s = "-91283472332"
    Output: -2147483648
    */

    def myAtoi(str: String): Int = {

      var result = 0

      for(i <- str)
        {
          var units = i - '0'
          result = result * 10 + units
        }

      result
    }

    //##################################################################################################################

    var input1 = "42"
    //var input2 = "         -42"
    //var input3 = "4193 with words"
    //var input4 = "words and 987"
    //var input5 = "-91283472332"

    var result1 = myAtoi(input1)
    //var result2 = myAtoi(input2)
    //var result3 = myAtoi(input3)
    //var result4 = myAtoi(input4)
    //var result5 = myAtoi(input5)

    println(result1)
    //println(result2)
    //println(result3)
    //println(result4)
    //println(result5)

  }
}
