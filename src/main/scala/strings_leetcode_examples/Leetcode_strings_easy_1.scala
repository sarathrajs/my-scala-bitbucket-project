package strings_leetcode_examples

object Leetcode_strings_easy_1 {

  def main(args:Array[String]) : Unit = {

    /*
    Reverse String:
    Write a function that reverses a string. The input string is given as an array of characters char[].
    Do not allocate extra space for another array, you must do this by modifying the input array in-place with O(1) extra memory.
    You may assume all the characters consist of printable ascii characters.

    Example 1:
    Input: ["h","e","l","l","o"]
    Output: ["o","l","l","e","h"]
    */

    def reverseString(s: Array[Char]): Unit = {

      println("Reverse String")
      var start = 0
      var end = s.length - 1

      while ( start < end )
        {
          var temp = s(start)
          s(start) = s(end)
          s(end) = temp
          start += 1
          end -= 1
        }

      s.foreach(println)
    }

    //##################################################################################################################

    var input = Array('h','e','l','l','o')
    reverseString(input)

  }
}
