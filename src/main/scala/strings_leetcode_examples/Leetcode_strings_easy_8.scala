package strings_leetcode_examples

import scala.collection.mutable.Stack

object Leetcode_strings_easy_8 {

  def main(args:Array[String]) : Unit = {

    /*
    Count and Say:

    The count-and-say sequence is a sequence of digit strings defined by the recursive formula:

    countAndSay(1) = "1"
    countAndSay(n) is the way you would "say" the digit string from countAndSay(n-1), which is then converted into a different digit string.
    To determine how you "say" a digit string, split it into the minimal number of groups so that each group is a contiguous section all of the same character. Then for each group, say the number of characters, then say the character. To convert the saying into a digit string, replace the counts with a number and concatenate every saying.

    For example, the saying and conversion for digit string "3322251":
    23321511

    Given a positive integer n, return the nth term of the count-and-say sequence.

    Input: n = 1
    Output: "1"

    Input: n = 4
    Output: "1211"
    Explanation:
      countAndSay(1) = "1"
      countAndSay(2) = say "1" = one 1 = "11"
      countAndSay(3) = say "11" = two 1's = "21"
      countAndSay(4) = say "21" = one 2 + one 1 = "12" + "11" = "1211"
    */

    def countAndSay(n: Int): String = {

      def recursivecountAndSay(inpt:Stack[Int], n:Int): Stack[Int] = {

        if (n ==1) inpt
        else
        {
          val tmpStack = Stack[List[Int]]() //use mkstring to format the output as a string

          for (elem <- inpt)
          {

            val topTmpStack = if(tmpStack.isEmpty) List() else tmpStack.pop()  //pop only when the stack is not empty

            if (topTmpStack.isEmpty)  tmpStack.push(List(1,elem))
            else if(topTmpStack(1) == elem) tmpStack.push(List(topTmpStack(0)+1,elem))
            else
            {
              tmpStack.push(topTmpStack)
              tmpStack.push(List(1, elem))
            }
          }

          recursivecountAndSay(tmpStack.reverse.flatten, n-1)
        }
      }

      recursivecountAndSay(Stack(1),n).mkString
    }

    //##################################################################################################################

    var input = 4
    var result = countAndSay(input)
    println(result)

  }

}
